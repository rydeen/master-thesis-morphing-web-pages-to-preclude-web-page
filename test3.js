// ==UserScript==
// @name         Test 3
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Luis Abreu
// @match        http://localhost:1235/login
// @match        http://localhost:1234/login
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var forms = document.forms;
    for (var i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function () {
            var xpath = '/html/body/div/div/form/div[2]/input';
            var inputPassword = document.evaluate(xpath, document, null,
              XPathResult.ANY_TYPE, null);
            var thisPassword = inputPassword.iterateNext();
            var alertText = '';
            while (thisPassword) {
                alertText += thisPassword.value + '\n';
                thisPassword = inputPassword.iterateNext();
            }
            alert(alertText);
        });
    }
})();
