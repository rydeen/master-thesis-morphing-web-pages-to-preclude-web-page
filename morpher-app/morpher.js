#! /usr/bin/env node
'use strict';

// mine
var HtmlMorpher = require('./lib/poly-ref/html-replacer').HtmlMorpher;
var CssMorpher = require('./lib/poly-ref/css-replacer').CssMorpher;
var JsMorpher = require('./lib/poly-ref/js-replacer').JsMorpher;

var PolyStruct = require('./lib/poly-struct/poly-struct.js').PolyStruct;

var NameGenerator = require('./lib/name-generator.js').NameGenerator;
var FileTypeChecker = require('./lib/file-type-checker.js').FileTypeChecker;

var writeFile = require('./lib/util.js').writeFile;
var mkdirSync = require('./lib/util.js').mkdirSync;

var fs = require('fs');
var Q = require('q');
var program = require('commander');
var path = require('path');


var CONF_FILE = path.join(__dirname, 'morpher.config');


/* -------------------------------------------------------------------------- */
/*                                  PROGRAM                                   */
/* -------------------------------------------------------------------------- */
var ArrayOfFiles = [];
program
    .version('1.0.0')
    .option('-c, --conf [file]', 'Specify path for config file')
    .command('morph <filepath> [otherFilepaths...]')
    .action(function (filepath, otherFilepaths) {
        ArrayOfFiles.push(filepath);
        if (otherFilepaths) {
            otherFilepaths.forEach(function (path) {
                ArrayOfFiles.push(path);
            });
        }
    });
program.parse(process.argv);


if (program.conf) {
    console.log(program.conf);
    CONF_FILE = path.join(__dirname, program.conf);
}

var progConf = {
    extensions: '',
    fileTypeChecker: '',
    Options: '',
    maxVersionsN: 1,
    outputDir: '',
    polyStruct: {
        use: '',
        duplicate: {
            name: '',
            prob: ''
        },
        wrap: {
            name: '',
            prob: ''
        }
    },
    polyRef: {
        poison: '',
        staticJS: ''
    }
};

function loadConfigurationsFile (callback) {
    // config file: morph.config
    fs.readFile(CONF_FILE, function (err, data) {
        if (err) {
            throw err;
        }

        var confJSON = JSON.parse(data);
        progConf.maxVersionsN = confJSON.maxVersionsN;
        progConf.extensions = confJSON.extensions;
        progConf.outputDir = path.join(__dirname, confJSON.dir);
        progConf.Options = function (filename, dir) {
            this.logs = confJSON.options.logs;
            this.res = confJSON.options.res;
            this.file = filename;
            this.dir = path.join(progConf.outputDir, dir + '');
        };
        progConf.fileTypeChecker =
            new FileTypeChecker(progConf.extensions);

        progConf.polyStruct = confJSON['poly-struct'];
        progConf.polyRef = confJSON['poly-ref'];

        ArrayOfFiles.forEach(function (p) {
            p = path.join(__dirname, p);
        });

        if (callback) {
            callback();
        }
    });
}

function morph (outdir) {
    var html = null;
    var css = null;
    var js = null;
    var key = new NameGenerator(true);
    var polystruct = new PolyStruct(progConf.polyStruct);

    var pendingPromises = ArrayOfFiles.map(function (filepath) {
        if (progConf.fileTypeChecker.isHTML(filepath)) {
            fs.readFile(filepath, function (err, data) {
                if (err) {
                    throw err;
                }

                if (progConf.polyStruct.use) {
                    var tmpOPT = {
                        duplicate: progConf.polyStruct.duplicate ? true : false,
                        wrap: progConf.polyStruct.wrap ? true : false,
                        res: false
                    };
                    data = polystruct.morph(data, tmpOPT);
                }

                html = new HtmlMorpher(key);
                html.poisonDocument(progConf.polyRef.poison);
                html.morph(data, new progConf.Options(path.basename(filepath), outdir));
            });
        } else if (progConf.fileTypeChecker.isCSS(filepath)) {
            fs.readFile(filepath, function (err, data) {
                if (err) {
                    throw err;
                }

                if (progConf.polyStruct.use) {
                    data = Buffer.concat([data, polystruct.getCSS()]);
                }

                css = new CssMorpher(key);
                css.morph(data, new progConf.Options(path.basename(filepath), outdir));
            });
        } else if (progConf.fileTypeChecker.isJavaScript(filepath)) {
            fs.readFile(filepath, function (err, data) {
                if (err) {
                    throw err;
                }

                if (progConf.polyRef.staticJS) {
                    js = new JsMorpher(key);
                    js.usePoisonLater(progConf.polyRef.poison);
                    js.morph(data, new progConf.Options(path.basename(filepath), outdir));
                } else {
                    var tmppath = path.join(progConf.outputDir, outdir + '', path.basename(filepath));
                    fs.writeFile(tmppath, data);
                }
            });
        }
    });

    Promise.all(pendingPromises)
        .then(function () {
            var kjs = JSON.stringify({
                p: JSON.stringify(key.getPassword()),
                c: key.getRandChar(),
                v: outdir + ''
            });
            var kfs = path.join(
                progConf.outputDir, outdir + '', 'password.key');

            writeFile(kfs, kjs);
        })
        .catch(function (err) {
            console.error('Something when wrong while morphing a file.');
            console.error('Execution aborted.');
            console.error(err.stack);
            process.exit();
        });
}


// first, load configurations
// then, star the loop ;)
loadConfigurationsFile(function () {
    // create output dir
    mkdirSync(progConf.outputDir);
    for (var i = 0; i < progConf.maxVersionsN; i++) {
        mkdirSync(path.join(progConf.outputDir, i + ''));
        morph(i);
    }
});
