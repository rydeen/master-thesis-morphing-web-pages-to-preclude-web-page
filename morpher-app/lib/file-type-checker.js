'use strict';

var path = require('path');


module.exports.FileTypeChecker = function (extentions) {
    var defaultExtensions = {
        html: ['.html'],
        css: ['.css'],
        js: ['.js']
    };

    this.extentions = extentions || defaultExtensions;

    this.isHTML = function (filename) {
        var ext = path.extname(filename);
        for (var i = 0; i < this.extentions.html.length; i++) {
            if (this.extentions.html[i] === ext) {
                return true;
            }
        }
        return false;
    };

    this.isCSS = function (filename) {
        var ext = path.extname(filename);
        for (var i = 0; i < this.extentions.css.length; i++) {
            if (this.extentions.css[i] === ext) {
                return true;
            }
        }
        return false;
    };

    this.isJavaScript = function (filename) {
        var ext = path.extname(filename);
        for (var i = 0; i < this.extentions.js.length; i++) {
            if (this.extentions.js[i] === ext) {
                return true;
            }
        }
        return false;
    };

    this.setExtensions = function (newExtentions) {
        this.extentions = newExtentions || defaultExtensions;
    };
};
