'use strict';

// external libs
var Q = require('q');
var htmlparser = require('htmlparser2');
var html = require('htmlparser-to-html');
var path = require('path');

// mine
var readJsonFile = require('./../util.js').readJsonFile;
var writeFile = require('./../util.js').writeFile;
var mkdirSync = require('./../util.js').mkdirSync;
var logOperation = require('./../util.js').logOperation;


module.exports.HtmlMorpher = function (key) {
    var WhoAmI = 'HTML REPLACER';
    var encodingKey = Object.create(key);

    var _OPTIONS = {
        logs: false, // print logs to stdout ?
        res: true, // print results to stdout ?
        dir: null, // dir to create the output file
        file: null // name for the output file
    };

    var poisonDocument = false;

    this.poisonDocument = function (bool) {
        poisonDocument = bool;
    };

    function morphHTML (dom, filter) {
        for (var i = 0; i < dom.length; i++) {
            // a node is only interesting if it has attributes
            if (dom[i].attribs) {
                var tmpElem = dom[i].name;

                // 'filter' is a JSON object built from the external file
                //   with the information on attributes by tags and the global
                //   ones

                // so, if the current node is on the list, it means we need to
                //   to search for specific attributes
                if (filter[tmpElem]) {
                    for (var j = 0; j < filter[tmpElem].length; j++) {
                        var property1 = filter[tmpElem][j];
                        var skip = false;

                        // for every attribute registered on the filter
                        //   check if it is present on the node
                        if (dom[i].attribs[property1]) {
                            // if it's a target, we can only change if the value
                            //   is a framename, and not '_self', '_blank', etc
                            if (property1 === 'target') {
                                var regex = new RegExp(/^_/);

                                // if the value cannot be changed, skip
                                if (regex.test(dom[i].attribs[property1])) {
                                    skip = true;
                                }
                            }

                            if (!skip) {
                                // encrypts the value of the current attribute
                                var enc1 = encodingKey
                                    .encrypt(dom[i].attribs[property1]);

                                if (_OPTIONS.logs) {
                                    logOperation(dom[i].attribs[property1],
                                        enc1, WhoAmI);
                                }

                                // replaces the old value with the new one
                                dom[i].attribs[property1] = enc1;
                            }
                        }
                    }
                }

                // otherwise, look for global attributes (in all nodes)
                tmpElem = 'global-attrs';
                if (filter[tmpElem]) {
                    for (var m = 0; m < filter[tmpElem].length; m++) {
                        var property2 = filter[tmpElem][m];

                        // for every attribute registered on the filter
                        //   check if it is present on the node
                        if (dom[i].attribs[property2]) {
                            // encrypts the value of the current attribute
                            // but class atributes are a special case, it can
                            //   have multiple values!
                            if (property2 === 'class') {
                                var values =
                                        (dom[i].attribs[property2]).split(' ');
                                dom[i].attribs[property2] = '';

                                for (var v = 0; v < values.length; v++) {
                                    var enc = encodingKey.encrypt(values[v]);
                                    dom[i].attribs[property2] += enc + ' ';

                                    if (_OPTIONS.logs) {
                                        logOperation(values[v], enc, WhoAmI);
                                    }
                                }

                                dom[i].attribs[property2] =
                                    (dom[i].attribs[property2]).slice(0, -1);
                            } else {
                                var enc2 = encodingKey
                                    .encrypt(dom[i].attribs[property2]);

                                if (_OPTIONS.logs) {
                                    logOperation(dom[i].attribs[property2],
                                        enc2, WhoAmI);
                                }

                                // replaces the old value with the new one
                                dom[i].attribs[property2] = enc2;
                            }
                        }
                    }
                }
            }

            // recursive call. If a node has childreens, keep going
            if (dom[i].children) {
                morphHTML(dom[i].children, filter);
            }
        }
    }

    function optionsHandler (options) {
        if (options.hasOwnProperty('logs') && options.hasOwnProperty('res')
            && typeof(options.logs) === 'boolean'
            && typeof(options.res) === 'boolean') {
            _OPTIONS.logs = options.logs;
            _OPTIONS.res = options.res;

            if (options.hasOwnProperty('dir')
                && options.hasOwnProperty('file')) {
                _OPTIONS.dir = options.dir;
                _OPTIONS.file = options.file;

                if (_OPTIONS.file === null || _OPTIONS.dir === null) {
                    return 1;
                }

                if (_OPTIONS.file === '' || _OPTIONS.dir === '') {
                    return 2;
                }
            } else {
                _OPTIONS.dir = null;
                _OPTIONS.file = null;
            }
        } else {
            return -1;
        }

        return 0;
    }

    function scriptInjection (data) {
        var files = ['poison.js']; // ['poison.js', 'cr.js', 'ng.js'];
        var scriptTag = '<script type="text/javascript" src="p/<src>"></script>';
        var tmpstr = '';

        for (var i = 0; i < files.length; i++) {
            tmpstr += scriptTag.replace('<src>', files[i]);
        }

        var tag = '</head>';
        var index = data.indexOf(tag);
        return data.slice(0, index) + tmpstr + data.slice(index);
    }

    this.morph = function (file, options) {
        switch (optionsHandler(options)) {
            case -1:
                throw new Error('Invalid options. Specify {logs:bool, res:bool}');
            case 1:
                throw new Error('Invalid options. file and dir cannot be null.');
            case 2:
                throw new Error('Invalid options. file and dir cannot be empty.');
            case 0:
                break;
            default:
                throw new Error('Unexpect result of the options handler');
        }

        var deferred = Q.defer();
        var MyDOM;

        Q.nfcall(htmlparser.parseDOM, file)
            .then(function (result) {
                MyDOM = result;

                // reads the external file with the information about
                //  global attributes and tag attributes to look for
                readJsonFile('anchors.json')
                    .then(function (result) {
                        var config = result;
                        return config;
                    })
                    .then(function (config) {
                        // morphing the DOM
                        morphHTML(MyDOM, config);

                        // translate to html code and writes to stdout
                        var res = html(MyDOM);
                        if (poisonDocument) {
                            res = scriptInjection(res);
                        }

                        if (_OPTIONS.res) {
                            console.log(res);
                        }

                        if (_OPTIONS.dir !== null) {
                            var tmpPath =
                                path.join(_OPTIONS.dir, _OPTIONS.file);
                            mkdirSync(_OPTIONS.dir);
                            writeFile(tmpPath, res);
                        }

                        deferred.resolve(res);
                    })
                    .catch(function (err) {
                        console.error(err);
                        deferred.reject(err);
                    });
            })
            .catch(function (err) {
                console.error(err.stack);
            });

        return deferred.promise;
    };
};
