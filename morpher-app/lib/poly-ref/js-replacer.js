'use strict';

var esprima = require('esprima');           // generate AST from JS
var estraverse = require('estraverse');     // traverse AST
var escodegen = require('escodegen');       // generate JS from AST
var colors = require('colors');             // coloring output
var Q = require('q');
var path = require('path');

var readJsonFile = require('./../util.js').readJsonFile;
var writeFile = require('./../util.js').writeFile;
var mkdirSync = require('./../util.js').mkdirSync;
var logOperation = require('./../util.js').logOperation;


module.exports.JsMorpher = function (key) {
    var WhoAmI = 'JS REPLACER';
    var encodingKey = Object.create(key);

    var goingToUsePoisonLater = false;
    var poisonChar = '_';

    this.usePoisonLater = function (val) {
        goingToUsePoisonLater = val;
    };


    var _OPTIONS = {
        logs: false,
        res: true,
        dir: null,
        file: null
    };

    function optionsHandler (options) {
        if (options.hasOwnProperty('logs') && options.hasOwnProperty('res')
            && typeof(options.logs) === 'boolean'
            && typeof(options.res) === 'boolean') {
            _OPTIONS.logs = options.logs;
            _OPTIONS.res = options.res;

            if (options.hasOwnProperty('dir') &&
                options.hasOwnProperty('file')) {
                _OPTIONS.dir = options.dir;
                _OPTIONS.file = options.file;

                if (_OPTIONS.file === null || _OPTIONS.dir === null) {
                    return 1;
                }

                if (_OPTIONS.file === '' || _OPTIONS.dir === '') {
                    return 2;
                }
            } else {
                _OPTIONS.dir = null;
                _OPTIONS.file = null;
            }
        } else {
            return -1;
        }

        return 0;
    }

    // -----------------------------------------------------------------------//
    // SCOPE OBJECT
    var JsFileScope = {
        scopeLevel: 0,
        scopes: [],
        isInit: false,
        // to call at the very begining (like a constructor)
        init: function () {
            this.scopeLevel = 0;
            this.createNewScope(this.scopeLevel);
            this.isInit = true;
        },
        // call at the very end (like a destructor)
        end: function () {
            if (this.scopeLevel !== 0) {
                throw new Error('Something went wrong. Scope pointer not zero');
            } else {
                this.scopes = null;
            }
        },
        // create a new position on the array 'scopes' and initialize it
        createNewScope: function (level) {
            if (level === null || typeof level !== 'number'
                || (typeof level === 'number' && level < 0)) {
                throw new Error('Invalid argument. Level must be int>=0.');
            } else {
                this.scopes[level] = {
                    variables: [],
                    documentRef: ['document']
                };
            }
        },
        // deletes a position on the array 'scopes'. the value becomes null
        deleteScope: function (level) {
            if (level === null || typeof level !== 'number'
                || (typeof level === 'number' && level < 0)) {
                throw new Error('Invalid argument. Level must be int>=0.');
            } else {
                this.scopes[level] = null;
            }
        },
        // verifies if the 'value' existis inside the current scopeLevel
        exists: function (value) {
            if (!this.isInit) {
                throw new Error('JsFileScope not initialized. Call init first');
            } else if (value === null || value === '') {
                throw new Error('Invalid argument specified on exists');
            } else {
                for (var s = this.scopeLevel; s >= 0; s--) {
                    for (var i = 0; i < this.scopes[s].variables.length; i++) {
                        if (value === this.scopes[s].variables[i]) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        // verifies if the 'value' can be used as a reference to 'document'
        isDoc: function (value) {
            if (!this.isInit) {
                throw new Error('JsFileScope not initialized. Call init first');
            } else if (value === null || value === '') {
                throw new Error('Invalid argument specified on isDoc');
            } else {
                for (var s = this.scopeLevel; s >= 0; s--) {
                    var i;
                    for (i = 0; i < this.scopes[s].documentRef.length; i++) {
                        if (value === this.scopes[s].documentRef[i]) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        // adds a new 'value' to the current scope level
        add: function (value) {
            if (!this.isInit) {
                throw new Error('JsFileScope not initialized. Call init first');
            } else if (value === null || value === '') {
                throw new Error('Invalid argument specified on add');
            } else {
                this.scopes[this.scopeLevel].variables.push(value);
            }
        },
        // adds a new reference for 'document' to the current scope level
        registerDoc: function (doc) {
            if (!this.isInit) {
                throw new Error('JsFileScope not initialized. Call init first');
            } else if (doc === null || doc === '') {
                throw new Error('Invalid argument specified on registerDoc');
            } else {
                this.scopes[this.scopeLevel].documentRef.push(doc);
            }
        },
        // moves the pointer forwards
        incPointer: function () {
            this.scopeLevel = this.scopeLevel + 1;
        },
        // moves the pointer backwards
        decPointer: function () {
            if (this.scopeLevel === 0) {
                throw new Error('Cannot move pointer below 0.');
            } else {
                this.scopeLevel = this.scopeLevel - 1;
            }
        }
    };

    // -----------------------------------------------------------------------//
    // RETURNABLE OBJECT
    function ReturnableObject () {
        var node;
        var info;

        this.getInfo = function () {
            return info;
        };

        this.getNode = function () {
            return node;
        };

        this.setInfo = function (newinfo) {
            info = newinfo;
        };

        this.setNode = function (newnode) {
            node = newnode;
        };
    }

    // -----------------------------------------------------------------------//
    // PRINTER
    colors.setTheme({
        literal: 'white',
        identifier: 'yellow',
        docRef: 'cyan',
        elems: 'blue',
        other: 'red',
        fn: 'green'
    });

    function printThis (type, value) {
        // if (!_OPTIONS.logs && !_OPTIONS.res) {
        //     return;
        // } else if (_OPTIONS.res && type === 'Return') {
        //     console.log(colors.white(value));
        //     return;
        // } else if (!_OPTIONS.logs) {
        //     return;
        // }

        // if (type === 'Info') {
        //     process.stdout.write('\n============ INFORMATION ============');
        //     process.stdout.write('\n==  ');
        //     process.stdout.write(colors.literal('represents a literal '));
        //     process.stdout.write(colors.literal('value'));
        //     process.stdout.write('     ==\n==  ');
        //     process.stdout.write(colors.identifier('represents an ident'));
        //     process.stdout.write(colors.identifier('ifier name'));
        //     process.stdout.write('  ==\n==  ');
        //     process.stdout.write(colors.other('represents untreated types'));
        //     process.stdout.write('     ==\n==  ');
        //     process.stdout.write(colors.docRef('represents references to '));
        //     process.stdout.write(colors.docRef('doc'));
        //     process.stdout.write('   ==\n==  ');
        //     process.stdout.write(colors.elems('represents elements'));
        //     process.stdout.write('            ==\n==  ');
        //     process.stdout.write(colors.fn('represents a function name'));
        //     process.stdout.write('     ==\n===============================');
        //     process.stdout.write('======\n\nResults:\n');
        //     return;
        // }

        // if (type === 'Return') {
        //     return;
        // }

        // process.stdout.write('> \'');
        // switch (type) {
        //     case 'Literal':
        //         process.stdout.write(colors.literal(value));
        //         break;
        //     case 'Identifier':
        //         process.stdout.write(colors.identifier(value));
        //         break;
        //     case 'Function':
        //         process.stdout.write(colors.fn(value));
        //         break;
        //     case 'DocRef':
        //         process.stdout.write(colors.docRef(value));
        //         break;
        //     case 'Element':
        //         process.stdout.write(colors.elems(value));
        //         break;
        //     default:
        //         process.stdout.write(colors.other(value));
        //         break;
        // }
        // process.stdout.write('\'\n');
    }

    // -----------------------------------------------------------------------//
    // SEARCH FOR ATTRIBUTES IN JSON FILE
    var WantedAttributes = {
        jsonfile: '',
        isWanted: function (attr) {
            for (var p in this.jsonfile) {
                for (var i = 0; i < this.jsonfile[p].length; i++) {
                    if (this.jsonfile[p][i] === attr) {
                        return true;
                    }
                }
            }
            return false;
        },
        setJson: function (file) {
            this.jsonfile = file;
        }
    };

    // -----------------------------------------------------------------------//
    // PROCESSORS
    function processMemberExpression (node) {
        if (node.type !== 'MemberExpression') {
            throw new Error('This is not a MemberExpression node');
        }

        var returnObj = new ReturnableObject();

        var isElem = JsFileScope.exists(node.object.name);

        var property;

        switch (node.object.type) {
            case 'MemberExpression':
                returnObj = processMemberExpression(node.object);
                node.object = returnObj.getNode();

                if (returnObj.getInfo() === 'element' ||
                    returnObj.getInfo() === 'indexOfArray') {
                    if (node.property.type === 'Identifier') {
                        property = node.property.name;
                        switch (property) {
                            case 'parentNode':
                            case 'firstChild':
                            case 'lastChild':
                            case 'previousSibling':
                            case 'previousElementSibling':
                            case 'nextSibling':
                            case 'nextElementSibling':
                            case 'childNodes':
                            case 'children':
                                returnObj.setInfo('element');
                                break;
                            case 'className':
                            case 'id':
                                returnObj.setInfo('idclass');
                                break;
                            case 'innerHTML':
                                returnObj.setInfo('html');
                                break;
                            default:
                                returnObj.setInfo('');
                                break;
                        }
                    }
                }

                break;
            case 'CallExpression':
                returnObj = processCallExpression(node.object);
                node.object = returnObj.getNode();
                switch (returnObj.getInfo()) {
                    case 'item()':
                        if (node.property.type === 'Identifier') {
                            property = node.property.name;
                            switch (property) {
                                case 'parentNode':
                                case 'firstChild':
                                case 'lastChild':
                                case 'previousSibling':
                                case 'previousElementSibling':
                                case 'nextSibling':
                                case 'nextElementSibling':
                                case 'childNodes':
                                case 'children':
                                    returnObj.setInfo('element');
                                    break;
                                case 'className':
                                case 'id':
                                    returnObj.setInfo('idclass');
                                    break;
                                case 'innerHTML':
                                    returnObj.setInfo('html');
                                    break;
                                default:
                                    returnObj.setInfo('');
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }

                break;
            case 'Identifier':
                if (isElem) {
                    if (node.property.type === 'Identifier') {
                        property = node.property.name;
                        switch (property) {
                            case 'parentNode':
                            case 'firstChild':
                            case 'lastChild':
                            case 'previousSibling':
                            case 'previousElementSibling':
                            case 'nextSibling':
                            case 'nextElementSibling':
                            case 'childNodes':
                            case 'children':
                                returnObj.setInfo('element');
                                break;
                            case 'className':
                            case 'id':
                                returnObj.setInfo('idclass');
                                break;
                            case 'innerHTML':
                                returnObj.setInfo('html');
                                break;
                            default:
                                returnObj.setInfo('');
                                break;
                        }
                    } else if (node.property.type === 'Literal') {
                        property = node.property.value;
                        if (!isNaN(parseInt(property, 10))) {
                            // if the code reaches here means that the user is
                            //   trying to access an index from an array of
                            //   elemensts
                            returnObj.setInfo('indexOfArray');
                        }
                    }
                }
                break;
            default:
                break;
        }

        returnObj.setNode(node);
        return returnObj;
    }

    function processCallExpression (node) {
        if (node.type !== 'CallExpression') {
            throw new Error('This is not a CallExpression node');
        }

        var returnObj = new ReturnableObject();

        var isDoc;
        var isElem;

        switch (node.callee.type) {
            case 'MemberExpression':
                var wantedVar = node.callee.object.name;

                isDoc = JsFileScope.isDoc(wantedVar);
                isElem = JsFileScope.exists(wantedVar);

                if (!isDoc && !isElem) {
                    switch (node.callee.object.type) {
                        case 'Identifier':
                            returnObj.setNode(node);
                            return returnObj;
                        case 'CallExpression':
                            returnObj =
                                processCallExpression(node.callee.object);
                            node.callee.object = returnObj.getNode();
                            break;
                        case 'MemberExpression':
                            returnObj =
                                processMemberExpression(node.callee.object);
                            node.callee.object = returnObj.getNode();
                            break;
                        default:
                            break;
                    }
                }

                var property = node.callee.property.name;
                var args = node.arguments;
                var typeFlag = '';

                switch (property) {
                    case 'getElementById': break;
                    case 'getElementsByClassName':
                        typeFlag = 'stringofclasses';
                        break;
                    case 'querySelector':
                    case 'querySelectorAll':
                        typeFlag = 'cssquery';
                        break;
                    case 'getElementsByName': break;
                    case 'write':
                    case 'writeln':
                        typeFlag = 'html';
                        break;
                    case 'setAttribute':
                        typeFlag = 'setattr';
                        break;
                    case 'item':
                        returnObj.setInfo('item()');
                        typeFlag = 'skip';
                        break;
                    default:
                        typeFlag = 'skip';
                        break;
                }

                var encryptedValue = '';

                switch (typeFlag) {
                    case 'skip': break;
                    case 'stringofclasses':
                        if (args.length !== 1) {break;}

                        if (args[0].type === 'Literal') {
                            var tmpVal = args[0].value.split(/\s/);
                            for (var i = 0; i < tmpVal.length; i++) {
                                printThis('Literal', tmpVal[i]);
                                encryptedValue +=
                                    encodingKey.encrypt(tmpVal[i]);
                                encryptedValue += ' ';
                            }
                            encryptedValue = encryptedValue.slice(0, -1);

                            if (_OPTIONS.logs) {
                                logOperation(node.arguments[0].value,
                                    encryptedValue.slice(0, -1), WhoAmI);
                            }

                            node.arguments[0].value = encryptedValue;

                            if (goingToUsePoisonLater) {
                                node.callee.property.name = poisonChar +
                                    node.callee.property.name;
                            }
                        } else if (args[0].type === 'Identifier') {
                            printThis('Identifier', args[0].name);
                        }
                        break;
                    case 'setattr':
                        if (args.length !== 2) {break;}

                        var checkAttrTrue = false;

                        // attribute name
                        if (args[0].type === 'Literal') {
                            printThis('Literal', args[0].value);
                            checkAttrTrue =
                                WantedAttributes.isWanted(args[0].value);
                        } else if (args[0].type === 'Identifier') {
                            printThis('Identifier', args[0].name);
                        }

                        // value for the attribute
                        if (args[1].type === 'Literal') {
                            printThis('Literal', args[1].value);

                            if (checkAttrTrue) {
                                encryptedValue =
                                    encodingKey.encrypt(args[1].value);

                                if (_OPTIONS.logs) {
                                    logOperation(node.arguments[1].value,
                                        encryptedValue, WhoAmI);
                                }

                                node.arguments[1].value = encryptedValue;

                                if (goingToUsePoisonLater) {
                                    node.callee.property.name = poisonChar +
                                        node.callee.property.name;
                                }
                            }
                        } else if (args[1].type === 'Identifier') {
                            printThis('Identifier', args[1].name);
                        }

                        break;
                    case 'cssquery':
                        // Expected to be a CSS query selector
                        // Validate and process it as in css-replacer.js
                        break;
                    case 'html':
                        // Expect to be an HTML block
                        // Validate and process it as in html-replacer.js
                        break;
                    default:
                        if (args.length !== 1) {break;}

                        if (args[0].type === 'Literal') {
                            printThis('Literal', args[0].value);
                            encryptedValue =
                                encodingKey.encrypt(node.arguments[0].value);

                            if (_OPTIONS.logs) {
                                logOperation(node.arguments[0].value,
                                    encryptedValue, WhoAmI);
                            }

                            node.arguments[0].value = encryptedValue;
                            if (goingToUsePoisonLater) {
                                node.callee.property.name = poisonChar +
                                    node.callee.property.name;
                            }
                        } else if (args[0].type === 'Identifier') {
                            printThis('Identifier', args[0].name);
                        }
                        break;
                }
                break;
            case 'Identifier':
                printThis('Function', node.callee.name);
                break;
            default:
                break;
        }

        returnObj.setNode(node);
        return returnObj;
    }

    function processVariableDeclaration (node) {
        if (node.type !== 'VariableDeclaration') {
            throw new Error('This is not a VariableDeclaration node');
        }

        var returnObj = new ReturnableObject();

        var declarations = node.declarations;

        for (var i = 0; i < declarations.length; i++) {
            var wantedVar;
            var property;
            var args;
            var typeFlag;

            var isDoc;
            var isElem;

            var encryptedValue = '';

            if (!declarations[i].init) {
                break;
            }

            switch (declarations[i].init.type) {
                case 'CallExpression':
                    if (declarations[i].init.callee.type === 'MemberExpression') {
                        wantedVar = declarations[i].init.callee.object.name;

                        isDoc = JsFileScope.isDoc(wantedVar);
                        isElem = JsFileScope.exists(wantedVar);

                        if (!isDoc && !isElem) {
                            if (declarations[i].init.callee.object.type ===
                                'Identifier') {
                                returnObj.setNode(node);
                                return returnObj;
                            }

                            returnObj = processCallExpression(
                                declarations[i].init.callee.object);
                            node.declarations[i].init.callee.object =
                                returnObj.getNode();
                        }

                        property = declarations[i].init.callee.property.name;
                        args = declarations[i].init.arguments;

                        typeFlag = '';
                        switch (property) {
                            case 'getElementById': break;
                            case 'getElementsByClassName':
                                typeFlag = 'stringofclasses';
                                break;
                            case 'querySelector':
                                typeFlag = 'cssquery';
                                break;
                            case 'querySelectorAll':
                                typeFlag = 'cssquery';
                                break;
                            case 'getElementsByName': break;
                            case 'write':
                            case 'writeln':
                                typeFlag = 'html';
                                break;
                            default:
                                typeFlag = 'skip';
                                break;
                        }

                        if (args.length === 1) {
                            switch (typeFlag) {
                                case 'skip':
                                    break;
                                case 'stringofclasses':
                                    if (args[0].type === 'Literal') {
                                        var tmpVal = args[0].value.split(/\s/);
                                        for (var k = 0; k < tmpVal.length; k++) {
                                            printThis('Literal', tmpVal[k]);
                                            encryptedValue +=
                                                encodingKey.encrypt(tmpVal[k]) +
                                                ' ';
                                        }

                                        encryptedValue =
                                            encryptedValue.slice(0, -1);

                                        if (_OPTIONS.logs) {
                                            logOperation(
                                                node.declarations[i].init
                                                .arguments[0].value,
                                                encryptedValue, WhoAmI);
                                        }

                                        node.declarations[i].init
                                            .arguments[0].value = encryptedValue;

                                        if (goingToUsePoisonLater) {
                                            node.declarations[i].init.callee.
                                                property.name = poisonChar +
                                                node.declarations[i].init.
                                                callee.property.name;
                                        }
                                    } else if (args[0].type === 'Identifier') {
                                        printThis('Identifier', args[0].name);
                                    }
                                    break;
                                case 'cssquery':
                                    // Expected to be a CSS query selector
                                    // Validate and process as in
                                    //    css-replacer.js
                                    break;
                                case 'html':
                                    // Expected to be an HTML block
                                    // Validate and process as in
                                    //    html-replacer.js
                                    break;
                                default:
                                    if (args[0].type === 'Literal') {
                                        printThis('Literal', args[0].value);
                                        encryptedValue =
                                            encodingKey.encrypt(args[0].value);

                                        if (_OPTIONS.logs) {
                                            logOperation(
                                                node.declarations[i].init
                                                .arguments[0].value,
                                                encryptedValue, WhoAmI);
                                        }

                                        node.declarations[i].init
                                            .arguments[0].value = encryptedValue;

                                        if (goingToUsePoisonLater) {
                                            node.declarations[i].init.callee.
                                                property.name = poisonChar +
                                                node.declarations[i].init.
                                                callee.property.name;
                                        }
                                    } else if (args[0].type === 'Identifier') {
                                        printThis('Identifier', args[0].name);
                                    }
                                    break;
                            }

                            JsFileScope.add(declarations[i].id.name);
                            printThis('Element', declarations[i].id.name);
                        }
                    }
                    break;
                case 'Identifier':
                    isDoc = JsFileScope.isDoc(declarations[i].init.name);
                    if (isDoc) {
                        JsFileScope.registerDoc(declarations[i].id.name);
                        printThis('DocRef', declarations[i].id.name);
                    }
                    break;
                case 'MemberExpression':
                    wantedVar = declarations[i].init.object.name;

                    isDoc = JsFileScope.isDoc(wantedVar);
                    isElem = JsFileScope.exists(wantedVar);

                    if (!isDoc && !isElem) {
                        var res;
                        switch (declarations[i].init.object.type) {
                            case 'MemberExpression':
                                res = processMemberExpression(
                                    declarations[i].init.object);
                                if (res.getInfo() !== 'element') {
                                    returnObj.setNode(node);
                                    return returnObj;
                                }
                                break;
                            case 'CallExpression':
                                returnObj = processCallExpression(
                                    declarations[i].init.object);

                                node.declarations[i].init.object =
                                    returnObj.getNode();
                                break;
                            case 'Identifier':
                                returnObj.setInfo('ignore');
                                returnObj.setNode(node);
                                return returnObj;
                            default:
                                break;
                        }
                    }

                    property = declarations[i].init.property.name;

                    switch (property) {
                        case 'parentNode':
                        case 'firstChild':
                        case 'lastChild':
                        case 'previousSibling':
                        case 'previousElementSibling':
                        case 'nextSibling':
                        case 'nextElementSibling':
                        case 'childNodes':
                        case 'children':
                            JsFileScope.add(declarations[i].id.name);
                            printThis('Element', declarations[i].id.name);
                            break;
                        case 'documentElement':
                            if (isDoc) {
                                JsFileScope.
                                    registerDoc(declarations[i].id.name);
                                printThis('DocRef', declarations[i].id.name);
                            }
                            break;
                        case 'ownerDocument':
                            if (isElem) {
                                JsFileScope.
                                    registerDoc(declarations[i].id.name);
                                printThis('DocRef', declarations[i].id.name);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        returnObj.setNode(node);
        return returnObj;
    }

    function processBinaryExpression (node) {
        if (node.type !== 'BinaryExpression') {
            throw new Error('This is not a BinaryExpression node');
        }

        var returnObj = new ReturnableObject();

        // IGNORE FOR NOW
        // - the sum of the different encrypted inputs is not the same thing as
        // the encrypted result of the sum of all the inputs and by this,
        // BinaryExpressions can only be treated dinamically

        // var encryptedValue = '';

        // switch (node.left.type) {
        //     case 'Literal':
        //         printThis('Literal', node.left.value);
        //         encryptedValue = encodingKey.encrypt(node.left.value);
        //         node.left.value = encryptedValue;
        //         break;
        //     case 'Identifier':
        //         printThis('Identifier', node.left.name);
        //         break;
        //     case 'BinaryExpression':
        //         returnObj = processBinaryExpression(node.left);
        //         node.left = returnObj.getNode();
        //         break;
        //     default:
        //         break;
        // }

        // switch (node.right.type) {
        //     case 'Literal':
        //         printThis('Literal', node.right.value);
        //         encryptedValue = encodingKey.encrypt(node.right.value);
        //         node.right.value = encryptedValue;
        //         break;
        //     case 'Identifier':
        //         printThis('Identifier', node.right.name);
        //         break;
        //     case 'CallExpression':
        //         returnObj = processCallExpression(node.right);
        //         node.right = returnObj.getNode();
        //         break;
        //     default:
        //         break;
        // }

        returnObj.setNode(node);
        return returnObj;
    }

    function processAssignmentExpression (node) {
        if (node.type !== 'AssignmentExpression') {
            throw new Error('This is not an AssignmentExpression node');
        }

        var returnObj = new ReturnableObject();

        var res;
        var isDoc;
        var isElem;

        var encryptedValue = '';

        switch (node.left.type) {
            case 'MemberExpression':
                res = processMemberExpression(node.left);
                if (res.getInfo() === 'idclass') {
                    switch (node.right.type) {
                        case 'Literal':
                            printThis('Literal', node.right.value);
                            encryptedValue =
                                encodingKey.encrypt(node.right.value);

                            if (_OPTIONS.logs) {
                                logOperation(node.right.value,
                                    encryptedValue, WhoAmI);
                            }

                            node.right.value = encryptedValue;

                            if (goingToUsePoisonLater) {
                                node.left.property.name = poisonChar +
                                    node.left.property.name;
                            }
                            break;
                        case 'BinaryExpression':
                            returnObj = processBinaryExpression(node.right);
                            node.right = returnObj.getNode();
                            break;
                        case 'CallExpression':
                            returnObj = processCallExpression(node.right);
                            node.right = returnObj.getNode();
                            break;
                        case 'Identifier':
                            printThis('Identifier', node.right.name);
                            break;
                        default:
                            break;
                    }
                } else if (res.getInfo() === 'html') {
                    // Check if the content is HTML markup
                    // if (markup)
                    //      then process it the same way as html-replacer.js
                    // else continue
                }
                break;
            case 'Identifier':
                switch (node.right.type) {
                    case 'CallExpression':
                        if (node.right.callee.type === 'MemberExpression') {
                            returnObj = processCallExpression(node.right);
                            node.right = returnObj.getNode();

                            switch (node.right.callee.property.name) {
                                case 'getElementById':
                                case 'getElementsByClassName':
                                case 'querySelector':
                                case 'querySelectorAll':
                                case 'getElementsByName':
                                    JsFileScope.add(node.left.name);
                                    printThis('Element', node.left.name);           // ///////////////// 'Identifier'
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case 'MemberExpression':
                        switch (node.right.object.type) {
                            case 'CallExpression':
                                returnObj =
                                    processCallExpression(node.right.object);
                                node.right.object = returnObj.getNode();

                                isDoc =
                                    JsFileScope.isDoc(node.right.object.name);
                                isElem =
                                    JsFileScope.exists(node.right.object.name);

                                switch (node.right.property.name) {
                                    case 'parentNode':
                                    case 'firstChild':
                                    case 'lastChild':
                                    case 'previousSibling':
                                    case 'previousElementSibling':
                                    case 'nextSibling':
                                    case 'nextElementSibling':
                                    case 'childNodes':
                                    case 'children':
                                        JsFileScope.add(node.left.name);
                                        printThis('Element', node.left.name);
                                        break;
                                    case 'ownerDocument':
                                        if (isElem) {
                                            JsFileScope.
                                                registerDoc(node.left.name);
                                            printThis('DocRef', node.left.name);
                                        }
                                        break;
                                    case 'documentElement':
                                        if (isDoc) {
                                            JsFileScope.
                                                registerDoc(node.left.name);
                                            printThis('DocRef', node.left.name);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 'Identifier':
                                isDoc =
                                    JsFileScope.isDoc(node.right.object.name);
                                isElem =
                                    JsFileScope.exists(node.right.object.name);

                                if (isDoc || isElem) {
                                    switch (node.right.property.name) {
                                        case 'parentNode':
                                        case 'firstChild':
                                        case 'lastChild':
                                        case 'previousSibling':
                                        case 'previousElementSibling':
                                        case 'nextSibling':
                                        case 'nextElementSibling':
                                        case 'childNodes':
                                        case 'children':
                                            JsFileScope.add(node.left.name);
                                            printThis('Element', node.left.name);
                                            break;
                                        case 'ownerDocument':
                                            if (isElem) {
                                                JsFileScope.
                                                    registerDoc(node.left.name);
                                            }
                                            break;
                                        case 'documentElement':
                                            if (isDoc) {
                                                JsFileScope.
                                                    registerDoc(node.left.name);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        returnObj.setNode(node);
        return returnObj;
    }

    function processExpressionStatement (node) {
        if (node.type !== 'ExpressionStatement') {
            throw new Error('This is not an ExpressionStatement node');
        }

        var returnObj = new ReturnableObject();

        switch (node.expression.type) {
            case 'CallExpression':
                if (node.expression.callee.type === 'MemberExpression') {
                    returnObj = processCallExpression(node.expression);
                    node.expression = returnObj.getNode();
                }
                break;
            case 'AssignmentExpression':
                returnObj = processAssignmentExpression(node.expression);
                node.expression = returnObj.getNode();
                break;
            case 'MemberExpression':
                returnObj = processMemberExpression(node.expression);
                node.expression = returnObj.getNode();
                break;
            default:
                break;
        }

        returnObj.setNode(node);
        return returnObj;
    }

    this.morph = function (input, options) {
        switch (optionsHandler(options)) {
            case -1:
                throw new Error('Invalid options specify {logs:t/f, res:t/f}');
            case 1:
                throw new Error('Invalid options file and dir cannot be null');
            case 2:
                throw new Error('Invalid options file and dir cannot be empty');
            case 0:
                break;
            default:
                throw new Error('Unexpect result of the options handler');
        }

        var deferred = Q.defer();

        readJsonFile('anchors.json')
        .then(function (jsonfile) {
            WantedAttributes.setJson(jsonfile);

            printThis('Info');

            // ast generation
            var ast = esprima.parse(input);
            JsFileScope.init();
            var returnObj = new ReturnableObject();

            // traversing ast
            estraverse.traverse(ast, {
                enter: function (node, parent) {
                    switch (node.type) {
                        case 'ExpressionStatement':
                            returnObj = processExpressionStatement(node);
                            break;
                        case 'VariableDeclaration':
                            returnObj = processVariableDeclaration(node);
                            break;
                        case 'FunctionDeclaration':
                            JsFileScope.incPointer();
                            JsFileScope.createNewScope(JsFileScope.scopeLevel);
                            break;
                        default:
                            break;
                    }

                    if (returnObj.getNode() !== 'undefined') {
                        return returnObj.getNode();
                    }
                },
                leave: function (node, parent) {
                    switch (node.type) {
                        case 'FunctionDeclaration':
                            JsFileScope.deleteScope(JsFileScope.scopeLevel);
                            JsFileScope.decPointer();
                            break;
                        default:
                            break;
                    }
                }
            });

            JsFileScope.end();

            // code generation
            var newJS = escodegen.generate(ast);
            printThis('Return', newJS);

            if (_OPTIONS.res) {
                console.log(newJS);
            }

            if (_OPTIONS.dir !== null) {
                var tmpPath = path.join(_OPTIONS.dir, _OPTIONS.file);
                mkdirSync(_OPTIONS.dir);
                writeFile(tmpPath, newJS);
            }

            deferred.resolve(newJS);
        })
        .catch(function (err) {
            console.log(err.stack);
            deferred.reject(err);
        });

        return deferred.promise;
    };
};
