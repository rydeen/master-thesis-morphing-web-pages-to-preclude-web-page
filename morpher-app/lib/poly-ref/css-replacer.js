'use strict';

// external libs
var Q = require('q');
var merge = require('lodash.merge');
var css = require('css');
var path = require('path');
var CssSelectorParser = require('css-selector-parser').CssSelectorParser;

// mine
var readJsonFile = require('./../util.js').readJsonFile;
var writeFile = require('./../util.js').writeFile;
var mkdirSync = require('./../util.js').mkdirSync;
var logOperation = require('./../util.js').logOperation;

module.exports.CssMorpher = function (key) {
    var defaultAnchors = {
        'global-attrs': ['id', 'class', 'contextmenu'],
        a: ['download', 'name', 'target'],
        applet: ['code', 'name', 'object'],
        area: ['download', 'media', 'target'],
        base: ['target'],
        button: ['form', 'formaction', 'name', 'target', 'value'],
        fieldset: ['form', 'name'],
        form: ['name'],
        frame: ['name'],
        iframe: ['name', 'srcdoc'],
        img: ['usemap'],
        input: ['form', 'formaction', 'formtarget', 'list', 'pattern', 'name'],
        keygen: ['form', 'name'],
        label: ['for', 'form'],
        link: ['target'],
        map: ['name'],
        meter: ['form'],
        object: ['usemap', 'name', 'form'],
        option: ['value'],
        output: ['for', 'form', 'name'],
        param: ['name'],
        td: ['headers'],
        textarea: ['form', 'name'],
        th: ['headers'],
        track: ['label'],
        select: ['name']
    };

    var WhoAmI = 'CSS REPLACER';
    var encodingKey = Object.create(key);
    var selectorParser = new CssSelectorParser();
    selectorParser.registerNestingOperators('>', '+', '~');
    selectorParser.registerAttrEqualityMods('^', '$', '*', '~', '|');

    var _OPTIONS = {
        logs: false, // print logs to stdout ?
        res: true, // print results to stdout ?
        // if res=false, 'dir' and 'file' must be specified!
        // for best results select a full path!
        dir: null, // dir to create the output file
        file: null // name for the output file
    };


    function morphQueryAux (parsedSelector) { // browserify "getAux"
        var obj = {};

        if (parsedSelector.hasOwnProperty('rule')) {
            if (parsedSelector.rule.hasOwnProperty('id')) {
                var tmpId = parsedSelector.rule.id;
                var newId = encodingKey.encrypt(tmpId);
                obj[tmpId] = newId;
            }

            if (parsedSelector.rule.hasOwnProperty('classNames')) {
                var tmpClasses = parsedSelector.rule.classNames;
                for (var k = 0; k < tmpClasses.length; k++) {
                    var tmpClass = tmpClasses[k];
                    var newClass = encodingKey.encrypt(tmpClass);
                    obj[tmpClass] = newClass;
                }
            }

            var tmpVal;
            var newVal;
            var tmpAttr;
            var wantedElem;

            if (parsedSelector.rule.hasOwnProperty('attrs')) {
                if (parsedSelector.rule.attrs[0].operator
                    && parsedSelector.rule.attrs[0].operator === '=') {
                    if (parsedSelector.rule.tagName) {
                        if (defaultAnchors[parsedSelector.rule.tagName]) {
                            wantedElem = parsedSelector.rule.tagName;
                            for (var j = 0; j < defaultAnchors[wantedElem].length; j++) {
                                tmpAttr = parsedSelector.rule.attrs[0].name;
                                if (tmpAttr === defaultAnchors[wantedElem][j]) {
                                    tmpVal = parsedSelector.rule.attrs[0].value;
                                    newVal = encodingKey.encrypt(tmpVal);
                                    obj[tmpVal] = newVal;
                                }
                            }
                        }
                    } else {
                        var globalattrs = 'global-attrs';
                        for (var m = 0; m < defaultAnchors[globalattrs].length; m++) {
                            tmpAttr = parsedSelector.rule.attrs[0].name;
                            if (tmpAttr === defaultAnchors[globalattrs][m]) {
                                tmpVal = parsedSelector.rule.attrs[0].value;
                                newVal = encodingKey.encrypt(tmpVal);
                                obj[tmpVal] = newVal;
                            }
                        }
                    }
                }
            }

            if (parsedSelector.rule.hasOwnProperty('rule')) {
                var res = morphQueryAux(parsedSelector.rule);
                merge(obj, res);
            }
        }

        return obj;
    }

    function morphQuery (queryString) { // browserify "get"
        var parsedSelectors;

        try {
            parsedSelectors = selectorParser.parse(queryString);
        } catch (err) {
            if (queryString.charAt(0) !== '@') {
                throw err;
            } else {
                return null;
            }
        }

        var arrayObj = [];

        if (parsedSelectors.hasOwnProperty('selectors')) {
            for (var i = 0; i < parsedSelectors.selectors.length; i++) {
                arrayObj.push(morphQueryAux(parsedSelectors.selectors[i]));
            }
        } else if (parsedSelectors.hasOwnProperty('rule')) {
            arrayObj.push(morphQueryAux(parsedSelectors));
        }

        merge.apply(null, arrayObj);

        var changes = arrayObj[0];

        for (var property in changes) {
            if (changes.hasOwnProperty(property)) {
                queryString = queryString.replace(property, changes[property]);
            }
        }

        return queryString;
    }

    function morphCSSAuxRules (astRules) {
        for (var i = 0; i < astRules.length; i++) {
            if (astRules[i].type === 'rule') {
                for (var k = 0; k < astRules[i].selectors.length; k++) {
                    // split rules with css selector parser
                    var tmpSelectors = astRules[i].selectors[k];
                    astRules[i].selectors[k] = morphQuery(tmpSelectors);
                }
            }
        }

        return astRules;
    }

    function morphCSS (ast) {
        for (var i = 0; i < ast.stylesheet.rules.length; i++) {
            var ruleType = ast.stylesheet.rules[i].type;
            if (ruleType === 'rule') {
                // for each selector, find all the rules and process each one
                for (var k = 0; k < ast.stylesheet.rules[i].selectors.length; k++) {
                    // split rules with css selector parser
                    var tmpSelectors = ast.stylesheet.rules[i].selectors[k];
                    ast.stylesheet.rules[i].selectors[k] = morphQuery(tmpSelectors);
                }
            } else if (ruleType === 'media' && ast.stylesheet.rules[i].rules) {
                var aux = morphCSSAuxRules(ast.stylesheet.rules[i].rules);
                ast.stylesheet.rules[i].rules = aux;
            }
        }

        return css.stringify(ast, {sourcemap: true}).code;
    }

    function optionsHandler (options) {
        if (options.hasOwnProperty('logs') && options.hasOwnProperty('res')
            && typeof(options.logs) === 'boolean'
            && typeof(options.res) === 'boolean') {
            _OPTIONS.logs = options.logs;
            _OPTIONS.res = options.res;

            if (options.hasOwnProperty('dir')
                && options.hasOwnProperty('file')) {
                _OPTIONS.dir = options.dir;
                _OPTIONS.file = options.file;

                if (_OPTIONS.file === null || _OPTIONS.dir === null) {
                    return 1;
                }

                if (_OPTIONS.file === '' || _OPTIONS.dir === '') {
                    return 2;
                }
            } else {
                _OPTIONS.dir = null;
                _OPTIONS.file = null;
            }
        } else {
            return -1;
        }

        return 0;
    }

    this.morph = function (file, options) {
        switch (optionsHandler(options)) {
            case -1:
                throw new
                    Error('Invalid options. Specify {logs:bool, res:bool}');
            case 1:
                throw new
                    Error('Invalid options. file and dir cannot be null.');
            case 2:
                throw new
                    Error('Invalid options. file and dir cannot be empty.');
            case 0:
                break;
            default:
                throw new
                    Error('Unexpect result of the options handler');
        }

        var deferred = Q.defer();
        var MyAST;

        MyAST = css.parse(file.toString());
        readJsonFile('anchors.json')
            .then(function (config) {
                // morphing and output export
                defaultAnchors = config;
                var res = morphCSS(MyAST);
                if (_OPTIONS.res) {
                    console.log(res);
                }

                if (_OPTIONS.dir !== null) {
                    var tmpPath = path.join(_OPTIONS.dir, _OPTIONS.file);
                    mkdirSync(_OPTIONS.dir);
                    writeFile(tmpPath, res);
                }

                deferred.resolve(res);
            })
            .catch(function (err) {
                console.error(err.stack);
                deferred.reject(err);
            });

        return deferred.promise;
    };

    this.query = morphQuery;
};
