'use strict';

// native libs
var fs = require('fs');
var path = require('path');
var colors = require('colors');

// external libs
var Q = require('q');

/**
 * Reads the content of the file specified by 'filepath'
 * @param filepath
 */
module.exports.readJsonFile = function (filepath) {
    return Q.nfcall(fs.readFile, path.join(__dirname, filepath))
        .then(function (result) {
            return JSON.parse(result);
        })
        .catch(function (err) {
            console.error(err.stack);
        });
};

/**
 * Writes the 'data' of the file specified by 'filepath'
 * @param filepath
 */
module.exports.writeFile = function (filepath, data) {
    return Q.nfcall(fs.open, filepath, 'w')
        .then(function (fd) {
            fs.write(fd, data, function (err) {
                if (err) {
                    console.error(err);
                }
                return;
            });
        })
        .catch(function (err) {
            console.error(err);
        });
};

/**
 * Creates directory
 * @param dirpath
 */
module.exports.mkdir = function (dirpath) {
    return Q.nfcall(fs.mkdir, dirpath)
        .catch(function (err) {
            // does the directory already exists? if no, log error
            if (err.code !== 'EEXIST') {
                console.error(err);
            }
            console.log('aqui');
        });
};

module.exports.mkdirSync = function (dirpath) {
    try {
        fs.mkdirSync(dirpath);
    } catch (err) {
        if (err.code !== 'EEXIST') {
            throw err;
        }
    }
};

/**
 *
 */
module.exports.logOperation = function (oldVal, newVale, who) {
    process.stdout.write(colors.yellow(who.toUpperCase()) + '\t');
    process.stdout.write(colors.red('old: '));
    process.stdout.write(colors.white(oldVal));
    process.stdout.write(colors.green(' > new: '));
    process.stdout.write(colors.white(newVale) + '\n');
};
