'use strict';

var htmlparser = require('htmlparser2');
var codegen = require('htmlparser-to-html');
var crypto = require('crypto');


var _OPTIONS = { // default values
    duplicate: true,
    wrap: true,
    res: true,
    dir: null,
    file: null
};

var config = { // default values
    MaxRandN: 256,
    duplicate: {
        name: 'duplicatedefault',
        prob: 0.5
    },
    wrap: {
        name: 'wrapdefault',
        prob: 0.5
    }
};

module.exports.PolyStruct = function (configFile) {
    var ast;

    /**
     * Method used to stringiy object (as a method to create a copy)
     */
    function JSONreplacer (key, value) {
        switch (key) {
            case 'next':
            case 'prev':
            case 'parent':
                return null; // the value of "key" will be null
            case 'id':
                return undefined; // the "key" will be deleted from object
            default:
                return value;
        }
    }

    /**
     * Random Array Shuffle
     */
    function fisherYatesArrayShuffle (array) {
        var it = array.length;
        if (it === 0) {
            return false;
        }

        while (--it) {
            var rand = Math.floor(Math.random() * (it + 1));
            var tmp1 = array[it];
            var tmp2 = array[rand];
            array[it] = tmp2;
            array[rand] = tmp1;
        }

        return true;
    }

    /* -------------------------- CONFIGURATIONS ---------------------------- */
    function loadConfigurations (conf) {
        if (conf === {}) {
            return false;
        }

        if (conf.duplicate) {
            config.duplicate.name = conf.duplicate.cssClass;
            config.duplicate.prob = conf.duplicate.prob;
        }

        if (conf.wrap) {
            config.wrap.name = conf.wrap.cssClass;
            config.wrap.prob = conf.wrap.prob;
        }

        return true;
    }

    /* --------------------------- MAIN METHODS ----------------------------- */

    /**
     * Returns true or false, deciding if an action should be performed or not
     */
    function decideWhatToDo (who) {
        // if the property does not exists, return false
        if (!config[who]) {
            return false;
        }

        // generates a random byte -> hex -> decimal. It will be [0,256)
        var tmpRand = parseInt(crypto.randomBytes(1).toString('hex'), 16);
        // if the random number is in the first P(duplicate) numbers, doit
        var doit = tmpRand <= config.MaxRandN * config[who].prob;

        return doit;
    }

    function duplicate (ast) {
        for (var i = 0; i < ast.length; i++) {
            var doit = decideWhatToDo('duplicate');
            var currentElement = ast[i];

            // now, if we should duplicate, make sure the current  element
            //   is a tag but "html", "head" or "body" since we don't want
            //   to duplicate these.

            if (doit && currentElement.type === 'tag' && currentElement.name !== 'html' &&
                currentElement.name !== 'head' && currentElement.name !== 'body') {
                // make a copy of the current element (not a reference!)
                var tmp = JSON.parse(JSON.stringify(currentElement, JSONreplacer));

                // add a new class to apply "displat: none"
                if (tmp.attribs.class) {
                    var tmpArray = tmp.attribs.class.split(/\s/);
                    tmpArray.push(config.duplicate.name);
                    fisherYatesArrayShuffle(tmpArray); // shuffle all classes
                    tmpArray = tmpArray.toString().replace(/,/g, ' ');
                    tmp.attribs.class = tmpArray;
                } else {
                    // if there is no class attribute, add the new one
                    tmp.attribs.class = config.duplicate.name;
                }

                // the ids must be removed to avoid conflits
                if (tmp.attribs.id) {
                    delete tmp.attribs.id;
                }

                // add the duplicated node next to the original
                if (currentElement.parent && currentElement.parent.children) {
                    var nchilds = currentElement.parent.children.length;
                    var randInt = parseInt(crypto.randomBytes(1).toString('hex'), 16);
                    var position = Math.floor(randInt * nchilds / 255);
                    currentElement.parent.children.splice(position, 0, tmp);
                }
            }


            // if an element as children, process them unless the element
            //   in question is the head! (we don't want to mess with the
            //   head information)
            if (currentElement.children && currentElement.name !== 'head') {
                duplicate(currentElement.children);
            }
        }
    }

    function wrap (ast) {
        var doit;

        for (var i = 0; i < ast.length; i++) {
            doit = decideWhatToDo('wrap');

            // now, if we should wrap, make sure the current element is a
            //   tag but "html", "head" or "body" since we don't want  to
            //   wrap these.
            if (doit && ast[i].type === 'tag' && ast[i].name !== 'html'
                && ast[i].name !== 'head' && ast[i].name !== 'body') {
                var tmp = JSON.parse(JSON.stringify(ast[i], JSONreplacer));

                // the wrapper will be a "div" with class config.wrapClass
                ast[i] = {
                    type: 'tag',
                    name: 'div',
                    attribs: {
                        class: config.wrap.name
                    },
                    children: [tmp],
                    next: ast[i].next,
                    prev: ast[i].prev,
                    parent: ast[i].parent
                };
            }

            // if an element as children, process them unless the element
            //   in question is the head! (we don't want to mess with the
            //   head information)
            if (ast[i].children && ast[i].name !== 'head') {
                wrap(ast[i].children);
            }
        }
    }

    this.morph = function (sourcefile, opts) {
        _OPTIONS.duplicate = opts.duplicate;
        _OPTIONS.wrap = opts.wrap;
        _OPTIONS.dir = opts.dir;
        _OPTIONS.file = opts.file;
        _OPTIONS.res = opts.res;

        ast = htmlparser.parseDOM(sourcefile);

        if (_OPTIONS.duplicate) {
            duplicate(ast);
        }

        if (_OPTIONS.wrap) {
            wrap(ast);
        }

        var result = codegen(ast);

        if (_OPTIONS.res) {
            console.log(result);
        }

        if (_OPTIONS.dir && _OPTIONS.file) {
            console.log('devia criar ficheiro e gravar no disco');
        }

        return result;
    };

    this.getCSS = function () {
        var dupStyle = '\n.className {display: none;}';
        var wrapStyle = '\n.className {visibility: hidden; border: none; margin' +
                        ': 0; padding: 0; display: inline;} \n.class' +
                        'Name > * {visibility: visible;}\n';

        var regex = /className/g;

        var result = '';

        if (_OPTIONS.duplicate) {
            dupStyle = dupStyle.replace(regex, config.duplicate.name);
            result += dupStyle;
        }

        if (_OPTIONS.wrap) {
            wrapStyle = wrapStyle.replace(regex, config.wrap.name);
            result += wrapStyle;
        }

        return Buffer.from(result);
    };


    if (configFile) {
        loadConfigurations(configFile);
    }
};
