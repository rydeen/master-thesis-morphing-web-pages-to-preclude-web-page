'use strict';
var merge = require('lodash.merge');
var CssSelectorParser = require('css-selector-parser').CssSelectorParser;
module.exports = function (key) {
    var defaultAnchors = {
        'global-attrs': ['id', 'class', 'contextmenu'],
        a: ['download', 'name', 'target'],
        applet: ['code', 'name', 'object'],
        area: ['download', 'media', 'target'],
        base: ['target'],
        button: ['form', 'formaction', 'name', 'target', 'value'],
        fieldset: ['form', 'name'],
        form: ['name'],
        frame: ['name'],
        iframe: ['name', 'srcdoc'],
        img: ['usemap'],
        input: ['form', 'formaction', 'formtarget', 'list', 'pattern', 'name'],
        keygen: ['form', 'name'],
        label: ['for', 'form'],
        link: ['target'],
        map: ['name'],
        meter: ['form'],
        object: ['usemap', 'name', 'form'],
        option: ['value'],
        output: ['for', 'form', 'name'],
        param: ['name'],
        td: ['headers'],
        textarea: ['form', 'name'],
        th: ['headers'],
        track: ['label'],
        select: ['name']
    };

    var encodingKey = Object.create(key);

    var selectorParser = new CssSelectorParser();
    selectorParser.registerNestingOperators('>', '+', '~');
    selectorParser.registerAttrEqualityMods('^', '$', '*', '~', '|');

    function morphQueryAux (parsedSelector) { // browserify "getAux"
        var obj = {};

        if (parsedSelector.hasOwnProperty('rule')) {
            if (parsedSelector.rule.hasOwnProperty('id')) {
                var tmpId = parsedSelector.rule.id;
                var newId = encodingKey.encrypt(tmpId);
                obj[tmpId] = newId;
            }

            if (parsedSelector.rule.hasOwnProperty('classNames')) {
                var tmpClasses = parsedSelector.rule.classNames;
                for (var k = 0; k < tmpClasses.length; k++) {
                    var tmpClass = tmpClasses[k];
                    var newClass = encodingKey.encrypt(tmpClass);
                    obj[tmpClass] = newClass;
                }
            }

            var tmpVal;
            var newVal;
            var tmpAttr;
            var wantedElem;

            if (parsedSelector.rule.hasOwnProperty('attrs')) {
                if (parsedSelector.rule.attrs[0].operator
                    && parsedSelector.rule.attrs[0].operator === '=') {
                    if (parsedSelector.rule.tagName) {
                        if (defaultAnchors[parsedSelector.rule.tagName]) {
                            wantedElem = parsedSelector.rule.tagName;
                            for (var j = 0; j < defaultAnchors[wantedElem].length; j++) {
                                tmpAttr = parsedSelector.rule.attrs[0].name;
                                if (tmpAttr === defaultAnchors[wantedElem][j]) {
                                    tmpVal = parsedSelector.rule.attrs[0].value;
                                    newVal = encodingKey.encrypt(tmpVal);
                                    obj[tmpVal] = newVal;
                                }
                            }
                        }
                    } else {
                        var globalattrs = 'global-attrs';
                        for (var m = 0; m < defaultAnchors[globalattrs].length; m++) {
                            tmpAttr = parsedSelector.rule.attrs[0].name;
                            if (tmpAttr === defaultAnchors[globalattrs][m]) {
                                tmpVal = parsedSelector.rule.attrs[0].value;
                                newVal = encodingKey.encrypt(tmpVal);
                                obj[tmpVal] = newVal;
                            }
                        }
                    }
                }
            }

            if (parsedSelector.rule.hasOwnProperty('rule')) {
                var res = morphQueryAux(parsedSelector.rule);
                merge(obj, res);
            }
        }

        return obj;
    }

    function morphQuery (queryString) { // browserify "get"
        var parsedSelectors;

        try {
            parsedSelectors = selectorParser.parse(queryString);
        } catch (err) {
            if (queryString.charAt(0) !== '@') {
                throw err;
            } else {
                return null;
            }
        }

        var arrayObj = [];

        if (parsedSelectors.hasOwnProperty('selectors')) {
            for (var i = 0; i < parsedSelectors.selectors.length; i++) {
                arrayObj.push(morphQueryAux(parsedSelectors.selectors[i]));
            }
        } else if (parsedSelectors.hasOwnProperty('rule')) {
            arrayObj.push(morphQueryAux(parsedSelectors));
        }

        merge.apply(null, arrayObj);

        var changes = arrayObj[0];

        for (var property in changes) {
            if (changes.hasOwnProperty(property)) {
                queryString = queryString.replace(property, changes[property]);
            }
        }

        return queryString;
    }

    this.get = morphQuery;
};
