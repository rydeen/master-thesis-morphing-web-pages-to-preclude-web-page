'use strict';
var crypto = require('crypto');
module.exports = function () {
    var algorithm = 'aes-128-cbc';
    var password;
    var randChar;
    this.get = function (text) {
        var cipher = crypto.createCipher(algorithm, password);
        var crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return (randChar + crypted);
    };
    this.init = function (buffer, char) {
        if (buffer instanceof Buffer && char !== '') {
            password = buffer;
            randChar = char;
        } else {
            try {
                var parsedBuffer = JSON.parse(buffer);
                if (parsedBuffer.type === 'Buffer') {
                    password = new Buffer(parsedBuffer);
                    randChar = char;
                } else {
                    throw new Error('Password must be an instance of Buffer!');
                }
            } catch (err) {
                throw new Error('Password is not a valid JSON of type Buffer!');
            }
        }
    };
};
