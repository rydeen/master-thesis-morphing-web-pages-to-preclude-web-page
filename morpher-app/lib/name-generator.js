'use strict';

var crypto = require('crypto');


module.exports.NameGenerator = function (init) {
    var algorithm = 'aes-128-cbc';
    var password;
    var randChar;

    var defaultChar = 'a';
    var defaultPassword = new Buffer([120, 122, 57, 11, 186, 216, 43, 204, 205, 173,
        155, 207, 61, 98, 128, 131, 56, 186, 11, 93, 240, 139, 171, 81, 200, 20, 80,
        151, 133, 152, 9, 126, 234, 166, 31, 132, 6, 36, 109, 241, 241, 170, 101,
        100, 108, 83, 162, 61, 173, 117, 115, 16, 208, 76, 162, 83, 116, 106, 43,
        147, 106, 8, 104, 197, 37, 249, 157, 144, 111, 38, 49, 107, 43, 139, 72, 3,
        176, 91, 194, 25, 112, 8, 174, 211, 1, 7, 159, 16, 89, 244, 107, 63, 16,
        173, 214, 65, 164, 102, 133, 238, 20, 57, 231, 41, 243, 128, 222, 134, 153,
        5, 11, 8, 6, 215, 62, 119, 129, 80, 83, 3, 132, 221, 231, 35, 109, 60, 220,
        196]);

    function randomLowChar () {
        var low = 97;    // a
        var high = 123;  // z+1
        // [low,high)
        var charDecimal = Math.random() * (high - low) + low;
        return String.fromCharCode(charDecimal);
    }

    this.encrypt = function (text) {
        var cipher = crypto.createCipher(algorithm, password);
        var crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return (randChar + crypted);
    };

    this.decrypt = function (text) {
        text = text.substr(1);
        var decipher = crypto.createDecipher(algorithm, password);
        var dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    };

    this.generatePassword = function () {
        password = crypto.randomBytes(128);
        randChar = randomLowChar();
    };

    this.setPassword = function (buffer, char) {
        // PLEASE NOT THAT THE ARGUMENT 'char' IS NOT BEING CHECKED!!!
        if (buffer instanceof Buffer && char !== '') {
            password = buffer;
            randChar = char;
        } else {
            try {
                var parsedBuffer = JSON.parse(buffer);
                if (parsedBuffer.type === 'Buffer') {
                    password = new Buffer(parsedBuffer);
                    randChar = char;
                } else {
                    throw new Error('Password must be an instance of Buffer!');
                }
            } catch (err) {
                throw new Error('Password is not a valid JSON of type Buffer!');
            }
        }
    };

    this.getPassword = function () {
        if (!password) {
            return null;
        }

        return password;
    };

    this.getRandChar = function () {
        if (!randChar) {
            return null;
        }

        return randChar;
    };

    this.reset = function () {
        password = null;
        randChar = null;
    };

    if (init) {
        this.generatePassword();
    } else {
        this.setPassword(defaultPassword, defaultChar);
    }
};
