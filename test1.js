// ==UserScript==
// @name         Test 1
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Luis Abreu
// @match        http://localhost:1235/login
// @match        http://localhost:1234/login
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var password = document.querySelector('input[name="password"]');
    var forms = document.forms;
    for (var i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function () {
            if (password !== null && password.value !== '') {
                alert(password.value);
            } else {
                alert('Where is your password?');
            }
        });
    }
})();
