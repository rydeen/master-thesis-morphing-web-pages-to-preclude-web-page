'use strict';

var path = require('path');                 // add if not already using
var fs = require('fs');                     // add if not already using
var Cookies = require('cookies');           // add if not already using
var bodyParser = require('body-parser');    // add if not already using
var express = require('express');

// =============================================================================     <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
var NameGenerator =
    require('../../morpher-app/lib/name-generator.js').NameGenerator;

//==============================================================================
// NOTES TO VERSION CONTROL OF MORPHED FILES ===================================
//==============================================================================
// THIS NOTE APPLIES TO ALL ROUTES DELIVERING ANY WEB PAGE BUT HOME
//
// if using morpher.getVersion(), the version number will be
//   different from the one stored inside the client's cookie
//   so there are two options:
//
// --- 1. use morpher.getVersion() AND update cookie on the client
//
// var cookies = new Cookies(req, res);
// var keyfilepath = path.join(__dirname, '../', 'views',
//         configObj.website, morpher.getVersion(), 'password.key');
// fs.readFile(keyfilepath, function (err, data) {
//     if (err) {
//         throw err;
//     }
//     // update cookie
//     cookies.set('poison', data, {httpOnly: false});
//     /* 
//      * 1. calculate path to file
//      * 2. log operation (optional)
//      * 3. set headers to force no-caching
//      * 4. incremente morpher version with 'nextVersion()'
//      * 5. send response to client
//      */
// });
//
//
// --- 2. read the version from the client's cookie
//
// var extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
// extractVersion = JSON.parse('{' + extractVersion + '}').v;
// /*
//  * now, in path.join() use 'extractVersion' instead of 
//  *   morpher.getVersion()
//  */
//
//==============================================================================

// app/routes.js
module.exports = function (app, passport, configObj) {
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    // files composing the web site
    var files = {
        index: 'index.html',
        login: 'login.html',
        signup: 'signup.html',
        profile: 'profile.html'
    };

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // CONFIGURATION - Using polymorphism or not? ==============================    - added to handle morphed file version and
    // =========================================================================       to destinguish if using original or not
    // morphed version handler
    var morpher = {
        versions: 1,
        iterator: 0,
        nextVersion: function () {
            if (this.iterator + 1 < this.versions) {
                this.iterator += 1;
            } else {
                this.iterator = 0;
            }
        },
        getVersion: function () {
            return this.iterator + ''; // to return a string!
        }
    };

    if (configObj) {
        morpher.versions = configObj.versions;
        // a better approach should be a for loop for all properties in object
        files.index = configObj.website + '/<version>/' + files.index;
        files.login = configObj.website + '/<version>/' + files.login;
        files.signup = configObj.website + '/<version>/' + files.signup;
        files.profile = configObj.website + '/<version>/' + files.profile;
    } else {
        // a better approach should be a for loop for all properties in object
        files.index = 'original/' + files.index;
        files.login = 'original/' + files.login;
        files.signup = 'original/' + files.signup;
        files.profile = 'original/' + files.profile;
    }

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // HOME PAGE (with login links) ============================================    - updated to deliver cookies with password
    // =========================================================================    - updated to deliver original files or new ones
    app.get('/', function (req, res) {
        // add this piece of code in order to read the password if needed
        var tmpp = '';
        if (configObj) {
            var cookies = new Cookies(req, res);
            var keyfilepath = path.join(__dirname, '../' /* go back to root */,
                    'views' /* the dir with the website files stored */,
                    configObj.website, morpher.getVersion(), 'password.key');
            fs.readFile(keyfilepath, function (err, data) {
                if (err) {
                    throw err;
                }

                cookies.set('poison', data, {httpOnly: false});
                tmpp = path.join(__dirname, '../', 'views',
                    files.index.replace('<version>', morpher.getVersion()));

                res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
                res.setHeader('Pragma', 'no-cache');
                res.setHeader('Expires', '0');

                morpher.nextVersion();
                res.sendFile(tmpp);
            });
        } else {
            // load the index.ejs file
            tmpp = path.join(__dirname, '../', 'views', files.index);
            res.sendFile(tmpp);
        }
    });

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // LOGIN ===================================================================    - same
    // =========================================================================
    // show the login form
    app.get('/login', function (req, res) {
        // render the page and pass in any flash data if it exists
        var tmpp = '';
        if (configObj) {
            var extractVersion;
            try {
                extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
                extractVersion = JSON.parse('{' + extractVersion + '}').v;
            } catch (err) {
                extractVersion = morpher.getVersion();
            }

            tmpp = path.join(__dirname, '../', 'views',
                    files.login.replace('<version>', extractVersion));

            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', '0');

            res.sendFile(tmpp, {message: req.flash('loginMessage')});
            
            // var cookies = new Cookies(req, res);
            // var keyfilepath = path.join(__dirname, '../', 'views',
            //         configObj.website, morpher.getVersion(), 'password.key');
            // fs.readFile(keyfilepath, function (err, data) {
            //     if (err) {
            //         throw err;
            //     }
            //     cookies.set('poison', data, {httpOnly: false});
            //     tmpp = path.join(__dirname, '../', 'views',
            //         files.login.replace('<version>', morpher.getVersion()));

            //     res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            //     res.setHeader('Pragma', 'no-cache');
            //     res.setHeader('Expires', '0');

            //     morpher.nextVersion();

            //     res.sendFile(tmpp, {message: req.flash('loginMessage')});
            // });
        } else {
            tmpp = path.join(__dirname, '../', 'views', files.login);
            res.sendFile(tmpp, {message: req.flash('loginMessage')});
        }
    });

    // process the login form
    app.post('/login', translateFormFields, passport.authenticate('local-login', {
        // redirect to the secure profile section
        successRedirect: '/profile',
        // redirect back to the signup page if there is an error
        failureRedirect: '/login',
        // allow flash messages
        failureFlash: true
    }));

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // SIGNUP ==================================================================    - same
    // =========================================================================    - added middleware function to translate fields
    // show the signup form
    app.get('/signup', function (req, res) {
        // render the page and pass in any flash data if it exists
        var tmpp = '';
        if (configObj) {
            var extractVersion;
            try {
                extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
                extractVersion = JSON.parse('{' + extractVersion + '}').v;
            } catch (err) {
                extractVersion = morpher.getVersion();
            }

            tmpp = path.join(__dirname, '../', 'views',
                files.signup.replace('<version>', extractVersion));
            
            res.sendFile(tmpp, {message: req.flash('signupMessage')});

            // var cookies = new Cookies(req, res);
            // var keyfilepath = path.join(__dirname, '../', 'views',
            //         configObj.website, morpher.getVersion(), 'password.key');
            // fs.readFile(keyfilepath, function (err, data) {
            //     if (err) {
            //         throw err;
            //     }
            //     cookies.set('poison', data, {httpOnly: false});
            //     tmpp = path.join(__dirname, '../', 'views',
            //         files.signup.replace('<version>', morpher.getVersion()));

            //     res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            //     res.setHeader('Pragma', 'no-cache');
            //     res.setHeader('Expires', '0');

            //     morpher.nextVersion();

            //     res.sendFile(tmpp, {message: req.flash('signupMessage')});
            // });
        } else {
            tmpp = path.join(__dirname, '../', 'views', files.signup);
            res.sendFile(tmpp, {message: req.flash('signupMessage')});
        }
    });

    // process the signup form
    app.post('/signup', translateFormFields, passport.authenticate('local-signup', {
        // redirect to the secure profile section
        successRedirect: '/profile',
        // redirect back to the signup page if there is an error
        failureRedirect: '/signup',
        // allow flash messages
        failureFlash: true
    }));

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // PROFILE SECTION =========================================================    - same
    // =========================================================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/profile', isLoggedIn, function (req, res) {
        var tmpPage = files.profile.replace('<version>', morpher.getVersion());
        var tmpp = '';

        if (configObj) {
            var extractVersion;
            try {
                extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
                extractVersion = JSON.parse('{' + extractVersion + '}').v;
            } catch (err) {
                extractVersion = morpher.getVersion();
            }

            tmpp = path.join(__dirname, '../', 'views',
                files.profile.replace('<version>', extractVersion));

            fs.readFile(tmpp, function (err, data) {
                if (err) {
                    throw err;
                }
                
                var tag1 = 'display-user-id';
                var tag2 = 'display-user-email';
                var tag3 = 'display-user-password';
                var code = data.toString();
                code = code.replaceAll(tag1, req.user._id);
                code = code.replaceAll(tag2, req.user.local.email);
                code = code.replaceAll(tag3, req.user.local.password);

                morpher.nextVersion();

                res.send(code);
            });

            // var cookies = new Cookies(req, res);
            // var keyfilepath = path.join(__dirname, '../', 'views',
            //         configObj.website, morpher.getVersion(), 'password.key');
            // fs.readFile(keyfilepath, function (err, data) {
            //     if (err) {
            //         throw err;
            //     }
            //     cookies.set('poison', data, {httpOnly: false});
            //     tmpp = path.join(__dirname, '../', 'views',
            //         files.profile.replace('<version>', morpher.getVersion()));

            //     res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            //     res.setHeader('Pragma', 'no-cache');
            //     res.setHeader('Expires', '0');

            //     // get the user out of session and pass to template
            //     fs.readFile(tmpp, function (err, data) {
            //         if (err) {
            //             throw err;
            //         }
                    
            //         var tag1 = 'display-user-id';
            //         var tag2 = 'display-user-email';
            //         var tag3 = 'display-user-password';
            //         var index1 = data.indexOf(tag1);
            //         var index2 = data.indexOf(tag2);
            //         var index3 = data.indexOf(tag3);

            //         data =  data.slice(0, index1) + req.user._id
            //                 + data.slice(index1 + tag1.length, index2)
            //                 + req.user.local.email
            //                 + data.slice(index2 + tag2.length, index3)
            //                 + req.user.local.password
            //                 + data.slice(index3 + tag3.length);

            //         morpher.nextVersion();

            //         res.send(data);
            //     });
            // });

        } else {
            tmpp = path.join(__dirname, '../', 'views', files.profile);
            // get the user out of session and pass to template
            fs.readFile(tmpp, function (err, data) {
                if (err) {
                    throw err;
                }

                var tag1 = 'display-user-id';
                var tag2 = 'display-user-email';
                var tag3 = 'display-user-password';
                var index1 = data.indexOf(tag1);
                var index2 = data.indexOf(tag2);
                var index3 = data.indexOf(tag3);

                data =  data.slice(0, index1) + req.user._id
                        + data.slice(index1 + tag1.length, index2) + req.user.local.email
                        + data.slice(index2 + tag2.length, index3) + req.user.local.password
                        + data.slice(index3 + tag3.length);

                res.send(data);
            });
        }
    });

    // =========================================================================
    // LOGOUT ==================================================================
    // =========================================================================
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // MORPHER NEEDED ROUTES TO STATIC FILES ===================================    - added and is used only when polymorphism
    // =========================================================================       is active. used to deliver css and js files
    // aditional route to static files (css and js files only here)
    app.get(/\.js$/, function (req, res) {
        var tmpp = '';
        var reqfile = req.url.substr(1);

        if (configObj) {
            var extractVersion;
            try {
                extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
                extractVersion = JSON.parse('{' + extractVersion + '}').v;
            } catch (err) {
                extractVersion = morpher.getVersion();
            }

            tmpp = path.join(__dirname, '../views', configObj.website,
                             extractVersion, reqfile);
        } else {
            tmpp = path.join(__dirname, '../views/original/js', reqfile);
        }

        res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.setHeader('Pragma', 'no-cache');
        res.setHeader('Expires', '0');

        res.sendFile(tmpp);
    });

    app.get(/\.css$/, function (req, res) {
        var tmpp = '';
        var reqfile = req.url.substr(1);

        if (configObj) {
            var extractVersion;
            try {
                extractVersion = req.headers.cookie.match(/\"v\"\:\"([0-9]+)\"/)[0];
                extractVersion = JSON.parse('{' + extractVersion + '}').v;
            } catch (err) {
                extractVersion = morpher.getVersion();
            }

            tmpp = path.join(__dirname, '../views', configObj.website,
                             extractVersion, reqfile);
        } else {
            tmpp = path.join(__dirname, '../views/original/css', reqfile);
        }

        res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.setHeader('Pragma', 'no-cache');
        res.setHeader('Expires', '0');

        res.sendFile(tmpp);
    });

    // =========================================================================    <<<<<<<<<<<<   POLYMORPHISM     >>>>>>>>>>>>
    // MIDDLEWARE FUNCTION TO TRANSLATE MORPHED VALUES TO ORIGINAL ONES ========    - added this as the middleware function for
    // =========================================================================       translation of form fields
    // - Middleware: translateFormFields

    function translateFormFields (req, res, next) {
        if (configObj) { // using configObj means we are using morphed versions

            var translatedParams = {};

            var key = req.headers.cookie.match(/(\"p\"\:\"\{)(.*)(\}\")/)[0];
            key = JSON.parse('{' + key + '}').p;

            var password = new NameGenerator(false);
            password.setPassword(key, '');

            for (var property in req.body) {
                if(req.body[property] instanceof Array) {
                    var kkk = 0;
                    for (; kkk<req.body[property].length; kkk++){
                        if (req.body[property][kkk] !== '') {
                            translatedParams[password.decrypt(property)] = req.body[property][kkk];
                            break;
                        }
                    }
                } else {
                    translatedParams[password.decrypt(property)] = req.body[property];
                }
            }

            req.body = translatedParams;
         }
        
        return next();
    }
};

// route middleware to make sure a user is logged in
function isLoggedIn (req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }
    // if they aren't redirect them to the home page
    res.redirect('/');
}
