// server.js
'use strict';

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var configDB = require('./config/database.js');

var colors = require('colors');

var fs = require('fs');
var path = require('path');

// program definition to accept options ========================================
var program = require('commander');
program
    .version('1.0.0')
    .option('-o, --original', 'Deliver original website')
    .option('-p, --polymorph', 'Deliver polymorphic website')
    .option('-P, --port [port]', 'Specify server port')
    .parse(process.argv);

// read external configurations ================================================
var configObj = {};
function loadExternalConfigurations (callback) {
    var CONF_FILE = 'config/server.config';
    fs.readFile(CONF_FILE, function (err, data) {
        if (err) {
            throw err;
        }

        var tmpJSON = JSON.parse(data);
        configObj.port = tmpJSON.port;
        configObj.poisoning = tmpJSON.poisoning;
        configObj.website = tmpJSON.website;
        configObj.homepage = tmpJSON.homepage;
        configObj.anchors = tmpJSON.anchors;
        configObj.morpherConfFile = tmpJSON['morpher-conf-file'];

        // OVERRIDE EXTERNAL FILE PORT FILE IF SPECIFIED THROUGH COMMAND LINE
        if (program.port) {
            configObj.port = program.port;
        }

        var tmpPath = path.join(__dirname, configObj.morpherConfFile);
        fs.readFile(tmpPath, function (err, data) {
            if (err) {
                throw err;
            }

            configObj.versions = JSON.parse(data).maxVersionsN;
            tmpPath = path.join(__dirname, 'config/server.config');

            if (callback) {
                callback();
            }
        });
    });
}

// configuration ===============================================================
function runServer () {
    mongoose.connect(configDB.url); // connect to our database

    require('./config/passport')(passport); // pass passport for configuration

    // set up our express application
    app.use(morgan('dev')); // log every request to the console
    app.use(cookieParser()); // read cookies (needed for auth)
    app.use(bodyParser.json()); // get information from html forms
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // required for passport        -- session secret
    app.use(session({
        secret: 'ilovescotchscotchyscotchscotch',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
    app.use(flash()); // use connect-flash for flash messages stored in session

    // routes ==================================================================

    // routes to deliver static files injected by morpher-app
    app.use('/p', express.static(configObj.poisoning));
    // other static files needed by the website that are static (not morphed)
    app.use('/fonts', express.static('views/original/fonts'));

    // load our routes and pass in our app and fully configured passport
    if (program.polymorph) {
        require('./app/routes.js')(app, passport, configObj);
    } else /* if (program.original) */ {
        require('./app/routes.js')(app, passport);
    }

    // launch ==================================================================
    app.listen(configObj.port, function () {
        var str = 'Server running at http://localhost:' + configObj.port;
        console.log(colors.underline(colors.yellow(str)));
    });
}

logOption();

loadExternalConfigurations(runServer);


function logOption () {
    var str;
    if (program.polymorph) {
        str = '\n   ___   ___  ___    ___       __    \n  / _ | ' +
            '/ _ \\/ _ \\  / _ \\___  / /_ __\n / __ |/ ___/ ___/ / __' +
            '_/ _ \\/ / // /\n/_/ |_/_/  /_/    /_/   \\___/_/\\_, / ' +
            '\n                              /___/  \n';
    } else {
        str = '\n   ___   ___  ___    ____      _      _           ' +
            '__\n  / _ | / _ \\/ _ \\  / __ \\____(_)__ _(_)__  ___ _/ /' +
            '\n / __ |/ ___/ ___/ / /_/ / __/ / _ `/ / _ \\/ _ `/ / \n/_/' +
            ' |_/_/  /_/     \\____/_/ /_/\\_, /_/_//_/\\_,_/_/  \n      ' +
            '                        /___/                 \n';
    }

    console.log(colors.bold(colors.white(str)));
} 