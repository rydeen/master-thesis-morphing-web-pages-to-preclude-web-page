'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var program = require('commander');

var colors = require('colors');

program
    .version('1.0.0')
    .option('-P, --port [port]', 'Specify server port')
    .parse(process.argv);


var SERVER_PORT = program.port || 1234;
/* -------------------------------------------------------------------------- */
/*                                  SERVER                                    */
/* -------------------------------------------------------------------------- */
var serverConf = '../morpher-server/config/server.config';
var morpherConf = '../morpher-app/morpher.config';
var serverConfJSON = {};

var app = express();
app.use(bodyParser.json());

app.get('/', function (req, res) {
    fs.readFile('index.html', function (err, data) {
        if (err) {
            console.error(err);
            res.status(404).send();
        } else {
            res.setHeader('Content-Type', 'text/html');
            res.status(200).send(data);
        }
    });
});

app.get('/confm', function (req, res) {
    fs.readFile(morpherConf, function (err, data) {
        if (err) {
            console.error(err);
            res.status(404).send();
        } else {
            res.setHeader('Content-Type', 'application/json');
            data = JSON.parse(data);
            res.status(200).send({
                'logs': data.options.logs,
                'res': data.options.res,
                'outputDir': data.dir,
                'nversions': data.maxVersionsN,
                'extensions': data.extensions,
                'staticjs': data['poly-ref'].staticJS,
                'poison': data['poly-ref'].poison,
                'poly-struct': data['poly-struct']
            });
        }
    });
});

app.get('/confs', function (req, res) {
    fs.readFile(serverConf, function (err, data) {
        if (err) {
            console.error(err);
            res.status(404).send();
        } else {
            serverConfJSON = JSON.parse(data);
            res.setHeader('Content-Type', 'application/json');
            res.status(200).send({'homepage': serverConfJSON.homepage});
        }
    });
});

app.post('/saveconf', function (req, res) {
    var newConf = {
        options: {logs: req.body.logs, res: req.body.res},
        dir: req.body.outputDir,
        maxVersionsN: req.body.nversions,
        extensions: req.body.extensions,
        'poly-ref': {poison: req.body.poison, staticJS: req.body.staticjs}
    };

    if (req.body.polystruct.use) {
        newConf['poly-struct'] = {
            use: true,
            duplicate: {
                name: req.body.polystruct.duplicate.name,
                prob: req.body.polystruct.duplicate.prob / 100
            },
            wrap: {
                name: req.body.polystruct.wrap.name,
                prob: req.body.polystruct.wrap.prob / 100
            }
        };
    } else {
        newConf['poly-struct'] = {use: false};
    }

    fs.writeFile(morpherConf, JSON.stringify(newConf));

    serverConfJSON.homepage = req.body.homepage;
    serverConfJSON.website = req.body.outputDir;

    fs.writeFile(serverConf, JSON.stringify(serverConfJSON), function (err) {
        if (err) {
            res.status(200).send('false');
        } else {
            res.status(200).send('true');
        }
    });
});

app.use(express.static('.'));

logOption();

app.listen(SERVER_PORT, function () {
    var str = 'Server running at http://localhost:' + SERVER_PORT;
    console.log(colors.underline(colors.yellow(str)));
});


function logOption () {
    var str;
    str = '\n  _____          ____        _      __    __       _ __     \n' +
        ' / ___/__  ___  / _(_)__ _  | | /| / /__ / /  ___ (_) /____ \n' +
        '/ /__/ _ \\/ _ \\/ _/ / _ `/  | |/ |/ / -_) _ \\(_-</ / __/ -_)\n' +
        '\\___/\\___/_//_/_//_/\\_, /   |__/|__/\\__/_.__/___/_/\\__/\\__/ \n' +
        '                   /___/                                    '

    console.log(colors.bold(colors.white(str)));
} 


