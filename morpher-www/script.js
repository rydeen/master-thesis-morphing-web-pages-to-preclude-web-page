/* ========================================================================== */
/*                                  HANDLERS                                  */
/* ========================================================================== */

/*
 * FILE SELECTION --------------------------------------------------------------
 */
var inputFiles = document.querySelector('input#file');
var inputFilesLabel = inputFiles.nextElementSibling;
var inputFilesLabelVal = inputFilesLabel.innerHTML;
inputFiles.onchange = function (e) {
    var fileName = '';
    if (this.files && this.files.length > 0) {
        fileName = this.getAttribute('data-multiple-caption').
            replace('{count}', this.files.length);
    } else {
        fileName = e.target.value.split('\\').pop();
    }

    if (fileName) {
        inputFilesLabel.innerHTML =
            '<span class="lnr lnr-upload"></span>' + fileName;
    } else {
        inputFilesLabel.innerHTML = inputFilesLabelVal;
    }

    morpherForm.files = this.files;
}

/*
 * SELECT AND DESELECT BUTTON FUNCTIONS ----------------------------------------
 */
function selectButton (elem) {
    elem.className = 'opt-button selected';
}
function deselectButton (elem) {
    elem.className = 'opt-button';
}

/*
 * PRINT LOGS BUTTON -----------------------------------------------------------
 */
var logs = document.querySelectorAll('tr#logs > td > button');

logs[0].onclick = function () {
    selectButton(logs[0]);
    deselectButton(logs[1]);
    morpherForm.logs = true;
};

logs[1].onclick = function () {
    selectButton(logs[1]);
    deselectButton(logs[0]);
    morpherForm.logs = false;
};

/*
 * PRINT RES BUTTON ------------------------------------------------------------
 */
var res = document.querySelectorAll('tr#res > td > button');

res[0].onclick = function () {
    selectButton(res[0]);
    deselectButton(res[1]);
    morpherForm.res = true;
};

res[1].onclick = function () {
    selectButton(res[1]);
    deselectButton(res[0]);
    morpherForm.res = false;
};

/*
 * OUTPUT DIR FOR MORPHED VERSIONS ---------------------------------------------
 */
var outdir = document.querySelector('tr#outputdir > td > input');

function getOutputDir() {
    morpherForm.outputDir = outdir.value;
}

/*
 * NUMBER OF VERSIONS TO CREATE ------------------------------------------------
 */
var nversions = {
    input: document.querySelector('tr#nversions > td > input'),
    label: document.querySelector('tr#nversions > td > div')
};

function updateNVersionsValue () {
    nversions.label.textContent = nversions.input.value + ' versions';
    morpherForm.nversions = nversions.input.value;
}

/*
 * DO JS STATIC ANALYSIS BUTTON ------------------------------------------------
 */
var staticjs = document.querySelectorAll('tr#staticjs > td > button');

staticjs[0].onclick = function () {
    selectButton(staticjs[0]);
    deselectButton(staticjs[1]);
    morpherForm.staticjs = true;
};

staticjs[1].onclick = function () {
    selectButton(staticjs[1]);
    deselectButton(staticjs[0]);
    morpherForm.staticjs = false;
};

/*
 * USE POISON BUTTON -----------------------------------------------------------
 */
var poison = document.querySelectorAll('tr#poison > td > button');

poison[0].onclick = function () {
    selectButton(poison[0]);
    deselectButton(poison[1]);
    morpherForm.poison = true;
};

poison[1].onclick = function () {
    selectButton(poison[1]);
    deselectButton(poison[0]);
    morpherForm.poison = false;
};

/*
 * HTML, CSS AND JS EXTENSITONS ------------------------------------------------
 */

var extButtons = document
    .querySelectorAll('tr#extensions > td.input > button');
var extInputs = document
    .querySelectorAll('tr#extensions > td.input > input');

function removeExtension (elem) {
    var type = elem.getAttribute('data-extension');
    var i;

    var value = elem.firstChild.textContent.substr(1);

    if (type === 'html') {
        i = morpherForm.extensions.html.indexOf(value);
        morpherForm.extensions.html.splice(i,1);
    } else if (type === 'css') {
        i = morpherForm.extensions.css.indexOf(value);
        morpherForm.extensions.css.splice(i,1);
    } else if (type === 'js') {
        i = morpherForm.extensions.js.indexOf(value);
        morpherForm.extensions.js.splice(i,1);
    }

    elem.parentNode.removeChild(elem);
}

function createTagElement(value, type) {
    var newspan = document.createElement('span');
    newspan.setAttribute('class', 'tag lnr lnr-cross');
    newspan.setAttribute('data-extension', type);
    newspan.setAttribute('onclick', 'removeExtension(this)');
    var label = document.createElement('label');
    label.textContent = ' .' + value;
    newspan.appendChild(label);
    return newspan;
}

function addExtension(elem, type) {
    if (elem.value === '' || elem.value === null) {
        return;
    }

    if (type === 'html') {
        if (morpherForm.extensions.html.indexOf(elem.value) >= 0) {
            return;
        }
        morpherForm.extensions.html.push('.' + elem.value);
    } else if (type === 'css') {
        if (morpherForm.extensions.css.indexOf(elem.value) >= 0) {
            return;
        }
        morpherForm.extensions.css.push('.' + elem.value);
    } else if (type === 'js') {
        if (morpherForm.extensions.js.indexOf(elem.value) >= 0) {
            return;
        }
        morpherForm.extensions.js.push('.' + elem.value);
    }

    document.querySelector('div.tags')
        .appendChild(createTagElement(elem.value, type));

    elem.value = null;
}

extButtons[0].onclick = function () {addExtension(extInputs[0], 'html');}
extButtons[1].onclick = function () {addExtension(extInputs[1], 'css');}
extButtons[2].onclick = function () {addExtension(extInputs[2], 'js');}

/*
 * APPLY POLYSTRUCT BUTTON -----------------------------------------------------
 */
var polystruct = document.querySelectorAll('tr#polystruct > td > button');

polystruct[0].onclick = function () {
    selectButton(polystruct[0]);
    deselectButton(polystruct[1]);
    morpherForm.polystruct.use = true;

    document.querySelector('#duplicationProb').removeAttribute('data-hide');
    document.querySelector('#dupClass').removeAttribute('data-hide');
    document.querySelector('#wrappingProb').removeAttribute('data-hide');
    document.querySelector('#wrapClass').removeAttribute('data-hide');
};

polystruct[1].onclick = function () {
    selectButton(polystruct[1]);
    deselectButton(polystruct[0]);
    morpherForm.polystruct.use = false;

    document.querySelector('#duplicationProb')
        .setAttribute('data-hide', '');
    document.querySelector('#dupClass').setAttribute('data-hide', '');
    document.querySelector('#wrappingProb').setAttribute('data-hide', '');
    document.querySelector('#wrapClass').setAttribute('data-hide', '');
};

/*
 * DUPLICATION PROBABILITY -----------------------------------------------------
 */
var duplicationProb = {
    input: document.querySelector('tr#duplicationProb > td > input'),
    label: document.querySelector('tr#duplicationProb > td > div')
};

function updatedDuplicateProb () {
    duplicationProb.label.textContent = duplicationProb.input.value + '%';
    morpherForm.polystruct.duplicate.prob = duplicationProb.input.value;
}

/*
 * DUPLICATION CSS CLASS -------------------------------------------------------
 */
function validateClassName () {
   var regex = /[a-z]([a-z0-9\-\_]*)([a-z0-9]+)/;
   return regex.test(this.value);
}

var duplicationClass = document.querySelector('tr#dupClass > td > input');

duplicationClass.onchange = function () {
    if (validateClassName()) {
        if (morpherForm.polystruct.use
            && morpherForm.polystruct.duplicate.prob > 0) {
            morpherForm.polystruct.duplicate.name = duplicationClass.value;
        }
    }
}

/*
 * WRAPPING PROBABILITY --------------------------------------------------------
 */
var wrappingProb = {
    input: document.querySelector('tr#wrappingProb > td > input'),
    label: document.querySelector('tr#wrappingProb > td > div')
};

function updatedWrappingProb () {
    wrappingProb.label.textContent = wrappingProb.input.value + '%';
    morpherForm.polystruct.wrap.prob = wrappingProb.input.value;
}

/*
 * WRAPPING CSS CLASS ----------------------------------------------------------
 */
var wrappingClass = document.querySelector('tr#wrapClass > td > input');

wrappingClass.onchange = function () {
    if (validateClassName()) {
        if (morpherForm.polystruct.use
            && morpherForm.polystruct.wrap.prob > 0) {
            morpherForm.polystruct.wrap.name = wrappingClass.value;
        }
    }
}

/*
 * SAVE BUTTON -----------------------------------------------------------------
 */
var saveButton = document.querySelector('button.submit-button');

saveButton.onclick = function () {
    getOutputDir();

    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest_M.readyState === XMLHttpRequest.DONE) {
          var feed = document.querySelector('div#feedback');
            if (httpRequest_M.status === 200) {
                // success
                feed.removeAttribute('data-hide');
                feed.textContent = 'Everything is now saved! :)';
                feed.className = 'nav-msg-success';
            } else {
                // error
                feed.setAttribute('data-hide', '');
                feed.textContent = 'Something went wrong while saving.. :(';
                feed.className = 'nav-msg-error';
            }
        }
    };
    httpRequest.open('POST', '/saveconf');
    httpRequest.setRequestHeader("Content-Type", "application/json");
    httpRequest.send(JSON.stringify(morpherForm));
}
