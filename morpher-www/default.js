morpherForm = {
    files: [],
    logs: true,
    res: true,
    outputDir: '',
    extensions: {
        html: [],
        css: [],
        js: []
    },
    nversions: 1,
    staticjs: true,
    poison: true,
    polystruct: {
        use: true,
        duplicate: {
            name: 'dup',
            prob: 0.5
        },
        wrap: {
            name: 'wrap',
            prob: 0.5
        }
    }
};

httpRequest_M = new XMLHttpRequest();

httpRequest_M.onreadystatechange = function () {
    if (httpRequest_M.readyState === XMLHttpRequest.DONE) {
        if (httpRequest_M.status === 200) {
            var json = JSON.parse(httpRequest_M.responseText);

            // initial state
            if (json.logs) {logs[0].click();} else {logs[1].click();}
            if (json.res) {res[0].click();} else {res[1].click();}
            if (json.staticjs) {staticjs[0].click();} else {staticjs[1].click();}
            if (json.poison) {poison[0].click();} else {poison[1].click();}
            if (json['poly-struct'].use) {
                polystruct[0].click();
                duplicationProb.input.value = json['poly-struct'].duplicate.prob * 100;
                wrappingProb.input.value = json['poly-struct'].wrap.prob * 100;
                duplicationClass.value = json['poly-struct'].duplicate.cssClass;
                wrappingClass.value = json['poly-struct'].wrap.cssClass;
                updatedDuplicateProb();
                updatedWrappingProb();
            } else {
                polystruct[1].click();
                duplicationProb.input.value = 0;
                wrappingProb.input.value = 0;
                duplicationClass.value = null;
                wrappingClass.value = null;
            }

            outdir.value = json.outputDir || null;
            morpherForm.outputDir = outdir.value;

            nversions.input.value = json.nversions || 100;
            nversions.label.textContent = nversions.input.value + ' versions';
            morpherForm.nversions = nversions.input.value;

            json.extensions.html.forEach(function (t) {addExtension({value: t.substr(1)}, 'html');});
            json.extensions.css.forEach(function (t) {addExtension({value: t.substr(1)}, 'css');});
            json.extensions.js.forEach(function (t) {addExtension({value: t.substr(1)}, 'js');});
        } else {
            alert('There was a problem with the request.');
        }
    }
}

httpRequest_M.open('GET', '/confm');
httpRequest_M.send(null);
