// ==UserScript==
// @name         Test 2
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Luis Abreu
// @match        http://localhost:1235/login
// @match        http://localhost:1234/login
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var email = document.querySelector('input[type="text"]');
    // We need to apply the event listener to all forms because
    //  our tool may have replicated the original one
    var forms = document.forms;
    for (var i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function () {
            if (email !== null && email.value !== '') {
                alert(email.value);
            } else {
                alert('Where is your email?');
            }
        });
    }
})();
